﻿using UnityEngine;
using System.Collections;

public class PaddleMove : MonoBehaviour {

	public float Speed;

	void Update () {
		float VerticalValue = Input.GetAxis ("Vertical") * Speed * Time.deltaTime;
		this.transform.Translate (Vector3.up * VerticalValue);
	}
}
