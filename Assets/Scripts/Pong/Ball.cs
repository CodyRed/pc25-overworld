﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public float Speed = 30.0f;

	void Start () {
		GetComponent<Rigidbody2D> ().velocity = Vector2.right * Speed;
	}
	
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.name == "Player Paddle") {
			float y = hitFactor (transform.position, col.transform.position, 
				col.collider.bounds.size.y);
			Vector2 dir = new Vector2 (1, y).normalized;
			GetComponent<Rigidbody2D> ().velocity = dir * Speed;
		}

		if (col.gameObject.name == "AI Paddle") {
			float y = hitFactor (transform.position, col.transform.position, 
				col.collider.bounds.size.y);
			Vector2 dir = new Vector2 (-1, y).normalized;
			GetComponent<Rigidbody2D> ().velocity = dir * Speed;
		}

	}

	float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) {
		return (ballPos.y - racketPos.y) / racketHeight;
	}
}
