﻿using UnityEngine;
using System.Collections;

public class AITracker : MonoBehaviour {

	public GameObject Ball;
	public float Speed;

	void Start () {
	
	}

	void FixedUpdate() {
		if (this.transform.position.y < Ball.transform.position.y) {
			Vector3 direction = Vector3.up - Vector3.down;
			direction.Normalize ();
			transform.position += direction * Speed * Time.deltaTime;
		} else if (this.transform.position.y > Ball.transform.position.y) {
			Vector3 direction = Vector3.down - Vector3.up;
			direction.Normalize ();
			transform.position += direction * Speed * Time.deltaTime;
		} else {
			Vector3 direction = new Vector3 (0, 0, 0);
			transform.position += direction;
		}
	}
}
