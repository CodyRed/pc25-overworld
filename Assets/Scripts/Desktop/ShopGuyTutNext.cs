﻿using UnityEngine;
using System.Collections;

public class ShopGuyTutNext : MonoBehaviour {

	public GameObject PageOne;
	public GameObject PageTwo;
	private bool flip = false;

	void Update () {
		if (flip == false) {
			PageOne.SetActive (true);
			PageTwo.SetActive (false);
		} else {
			PageOne.SetActive (false);
			PageTwo.SetActive (true);
		}
	}

	public void FlipPage() {
		flip = !flip;
	}
}
