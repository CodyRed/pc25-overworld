﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextControl : MonoBehaviour {

    public Text time;
    public Text date;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //string text = System.DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");
        string timez = System.DateTime.Now.ToString("HH:mm:ss");
        string datez = System.DateTime.Now.ToString("MM/dd/yyyy");

        time.text = timez;
        date.text = datez;
    }
}
