﻿using UnityEngine;
using System.Collections;

public class TimedLife : MonoBehaviour {

	public float Duration;
	public GameObject PauseMaster;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			Duration -= 1 * Time.deltaTime;

			if (Duration <= 0) {
				Destroy (gameObject);
			}
		} else {
		return;
		}
	}
}
