﻿using UnityEngine;
using System.Collections;

public class PlaySingleSound : MonoBehaviour {
	public AudioClip PlayMe;
	void Start () {
		GetComponent<AudioSource> ().PlayOneShot (PlayMe);
	}
}
