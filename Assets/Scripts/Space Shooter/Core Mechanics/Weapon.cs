﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public string Name;
	public int Damage;
	public float Cooldown;
	private float maxcooldown;
	public float GeneratorDrain;
	public Bullet ProjectileOne;
	private Bullet currentbullet;
	public bool AirMode;

	void Start () {
		maxcooldown = Cooldown;
	}

	void Update () {
		Cooldown -= 1 * Time.deltaTime;
		if (Cooldown < 0) {
			Cooldown = 0;
		}
	}

	public virtual void Fire() {
		Cooldown = maxcooldown;
		currentbullet = Instantiate (ProjectileOne, transform.position, Quaternion.identity) as Bullet;
		currentbullet.GetComponent<Bullet> ().DidPlayerShoot ();
		currentbullet.GetComponent<Bullet> ().SetStrength (Damage);
		currentbullet.GetComponent<Bullet> ().AirMode = AirMode;
	}
}
