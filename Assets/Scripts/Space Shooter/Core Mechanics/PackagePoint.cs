﻿using UnityEngine;
using System.Collections;

public class PackagePoint : MonoBehaviour {

	public int CashReward;
	private int cashpenalty;
	public int Delivered;

	void Start () {
		Delivered = 1;
		cashpenalty = Mathf.RoundToInt(CashReward * 0.80f);
	}

	public void DeliveryDone()
	{
		Delivered--;
	}
}
