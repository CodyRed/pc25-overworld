﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	public float MyHealth;
	private float maxhealth;
	private GameObject Patcher;

	void Start()
	{
		Patcher = GameObject.Find ("SShooterMaster");
		if (Patcher != null) {
			if (gameObject.GetComponent < Enemy>() == null) {
				if (Patcher.GetComponent<SShooterSwitches> ().InfiniteHealth == true) {
					MyHealth = 100000;
					maxhealth = 100000;
				}
			}
		}
		maxhealth = MyHealth;
	}

	void Update()
	{
		if (Patcher != null) {
			if (gameObject.GetComponent <Enemy> () == null) {
				if (Patcher.GetComponent<SShooterSwitches> ().InfiniteHealth == true) {
					MyHealth = 100000;
					maxhealth = 100000;
				}
			}
		}
		if (MyHealth > maxhealth) {
			MyHealth = maxhealth;
		}
	}

	public float GetMaxHealth()
	{
		return maxhealth;
	}
}
