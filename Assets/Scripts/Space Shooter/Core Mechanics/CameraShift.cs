﻿using UnityEngine;
using System.Collections;

public class CameraShift : MonoBehaviour {

	private Quaternion groundrotate;

	void Start () {
		groundrotate = transform.rotation;
	}

	public void GroundMode()
	{
		Vector3.Lerp (transform.position, new Vector3 (0, 10, 0), 1.5f);
		transform.position = new Vector3(0, 10, 0);
		transform.rotation = groundrotate;
	}

	public void AirMode()
	{
		this.transform.position = new Vector3(0, 0, -10);
		transform.rotation = new Quaternion (0, 0, 0, transform.rotation.w);
	}
}

//GroundMode is above player at 0, 10, 0 facing 90, 0, 0.
//AirMode is away from player at 0, 0, -10 facing 0, 0, 0.