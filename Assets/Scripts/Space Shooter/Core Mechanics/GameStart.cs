﻿using UnityEngine;
using System.Collections;

public class GameStart : MonoBehaviour {

	public GameObject LoadText;

	void Start()
	{
		LoadText.SetActive (false);
	}

	public void StartGame()
	{
		LoadText.SetActive (true);
		Application.LoadLevel ("Level1");
	}
}
