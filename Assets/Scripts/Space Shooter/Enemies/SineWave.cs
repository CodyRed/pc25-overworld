﻿using UnityEngine;
using System.Collections;

//Base code from http://answers.unity3d.com/questions/803434/how-to-make-projectile-to-shoot-in-a-sine-wave-pat.html
public class SineWave : MonoBehaviour {

	public float MoveSpeed = 5.0f;
	public float frequency = 20.0f;
	public float magnitude = 0.5f;
	private Vector3 axis;
	private Vector3 pos;
	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		pos = transform.position;
		axis = transform.right;
		//I'll sort this out later. Right now objects with just the SineWave movement will move.
		if (gameObject.GetComponent<ScrollDown> () != null) {
			MoveSpeed = gameObject.GetComponent<ScrollDown>().Speed;
		}
	}
	
	void Update () {
		//Take note that this should be += for player projectiles (so it will go up instead)
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			pos -= transform.forward * Time.deltaTime * MoveSpeed;
			transform.position = pos + axis * Mathf.Sin (Time.time * frequency) * magnitude;
		}
	}
}