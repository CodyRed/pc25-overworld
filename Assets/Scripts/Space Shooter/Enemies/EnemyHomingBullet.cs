﻿using UnityEngine;
using System.Collections;

public class EnemyHomingBullet : MonoBehaviour {

	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		transform.LookAt(GameObject.FindGameObjectWithTag ("Player").transform.position);
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			transform.position += transform.forward * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
		}
	}
}
