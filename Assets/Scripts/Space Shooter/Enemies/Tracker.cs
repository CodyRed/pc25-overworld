﻿using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour {

	public float TrackTime;
	private float maxtracktime;
	public float LockTime;
	private float maxlocktime;
	public float FireTime;
	public float BarrageFireCooldown;
	private GameObject player;
	private GameObject currentbullet;
	private bool firing;
	private bool done;

	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		player = GameObject.FindGameObjectWithTag ("Player");
		maxtracktime = TrackTime;
		maxlocktime = LockTime;
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		//Fire!
		if (LockTime <= 0) {
			gameObject.GetComponent<Enemy>().BarrageFire(BarrageFireCooldown);
			FireTime -= 1 * Time.deltaTime;
		}

		if (FireTime <= 0) {
			done = true;
		}

		//Suicide when done.
		if (done)
		{
			LockTime = maxlocktime;
			TrackTime = maxtracktime;
			Vector3 direction = Vector3.forward - Vector3.back;
			direction.Normalize ();
			transform.position += direction * 4.0f * Time.deltaTime;
			Destroy(gameObject, 2.0f);
		}

		//If not locked, tick down tracking time.
		if (!firing) {
			TrackTime -= 1 * Time.deltaTime;
			if (TrackTime <= 0)
			{
				TrackTime = 0;
			}
		}

		//Follow player's X position only if not done.
		if (TrackTime > 0) {
			if (!done)
			{
				transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
			}
		}

		//Stop tracking and prepare to fire.
		if (TrackTime <= 0) {
			firing = true;
			LockTime -= 1 * Time.deltaTime;
			if (LockTime < 0)
			{
				LockTime = 0;
			}
		}
	}

	void Barrage()
	{
		currentbullet = Instantiate (gameObject.GetComponent<Enemy> ().MyBullet, 
		                             transform.position, Quaternion.identity) as GameObject;
	}
}
