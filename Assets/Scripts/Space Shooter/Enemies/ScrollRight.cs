﻿using UnityEngine;
using System.Collections;

public class ScrollRight : MonoBehaviour {

	public float Speed;
	private GameObject PauseMaster;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			Vector3 direction = Vector3.right - Vector3.left;
			direction.Normalize ();
			transform.position += direction * Speed * Time.deltaTime;
		}
	}
}
