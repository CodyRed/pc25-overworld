﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int Damage;
	public float AttackRange;
	public Bullet MyBullet;
	public float Cooldown;
	private float maxcooldown;
	public int CashReward;
	public GameObject player;
	public GameObject DeathSound;

	private GameObject PauseMaster;
	private Bullet currentbullet;

	// Use this for initialization
	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		maxcooldown = Cooldown;
		Cooldown = 0;
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		if (gameObject.GetComponent<Health>().MyHealth <= 0) {
			Instantiate (DeathSound, transform.position, Quaternion.identity);
			player.GetComponent<CashHandler> ().AddCash (CashReward);
			player.GetComponent<MurderTracker> ().AddMurder();
			Destroy (gameObject);
		}

		Cooldown -= 1 * Time.deltaTime;
		if (Cooldown < 0) {
			Cooldown = 0;
		}

		if (Vector3.Distance (transform.position, player.transform.position) <= AttackRange) {
			if (Cooldown <= 0)
			{
				currentbullet = Instantiate(MyBullet, transform.position, Quaternion.identity) as Bullet;
				currentbullet.GetComponent<Bullet>().SetStrength(Damage);
				Cooldown = maxcooldown;
			}
		}
	}

	public void BarrageFire(float cooldown)
	{
		maxcooldown = cooldown;
	}
}
