﻿using UnityEngine;
using System.Collections;

public class ShooterSpawner : MonoBehaviour {

	public Enemy MySpawn;
	public PackagePoint MyPackage;
	public bool PackageSpawner;
	public int Count;
	private int numberspawned;
	public float SpawnInterval;
	private float spawncooldown;
	public float SpawnProximity;
	public float SpawnPointX;
	public float SpawnPointY;
	public float SpawnPointZ;
	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		spawncooldown -= 1 * Time.deltaTime;
		if (spawncooldown < 0) {
			spawncooldown = 0;
		}

		if (Vector3.Distance (transform.position, new Vector3 (SpawnPointX, SpawnPointY, SpawnPointZ))
			<= SpawnProximity) 
		{
			if (spawncooldown <= 0)
			{
				if (!PackageSpawner)
				{
					Instantiate (MySpawn, transform.position, Quaternion.identity);
					spawncooldown = SpawnInterval;
					numberspawned++;
				}
				else
				{
					Instantiate (MyPackage, transform.position, Quaternion.identity);
					Destroy(gameObject);
				}
			}
		}

		if (numberspawned == Count) {
			Destroy(gameObject);
		}
	}
}
