﻿using UnityEngine;
using System.Collections;

public class ScrollDown : MonoBehaviour {

	public float Speed;
	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			Vector3 direction = Vector3.back - Vector3.forward;
			direction.Normalize ();
			transform.position += direction * Speed * Time.deltaTime;
		}
	}
}
