﻿using UnityEngine;
using System.Collections;

public class Trishot : Weapon {
	
	public Bullet LeftBullet;
	public Bullet RightBullet;
	private Bullet tsbullet;
	private Bullet tsbullet1;
	private Bullet tsbullet2;
	private float mcd;

	void Start () {
		mcd = Cooldown;
	}

	void Update () {
		Cooldown -= 1 * Time.deltaTime;
		if (Cooldown < 0) {
			Cooldown = 0;
		}
	}

	public override void Fire ()
	{
		Cooldown = mcd;
		tsbullet = Instantiate (ProjectileOne, transform.position, Quaternion.identity) as Bullet;
		tsbullet.GetComponent<Bullet> ().DidPlayerShoot ();
		tsbullet.GetComponent<Bullet> ().SetStrength (Damage);
		tsbullet.GetComponent<Bullet> ().AirMode = AirMode;
		tsbullet1 = Instantiate (LeftBullet, transform.position, Quaternion.identity) as Bullet;
		tsbullet1.GetComponent<Bullet> ().DidPlayerShoot ();
		tsbullet1.GetComponent<Bullet> ().SetStrength (Damage);
		tsbullet1.GetComponent<Bullet> ().AirMode = AirMode;
		tsbullet2 = Instantiate (RightBullet, transform.position, Quaternion.identity) as Bullet;
		tsbullet2.GetComponent<Bullet> ().DidPlayerShoot ();
		tsbullet2.GetComponent<Bullet> ().SetStrength (Damage);
		tsbullet2.GetComponent<Bullet> ().AirMode = AirMode;
	}
}
