﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tut : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Time.timeScale = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.E)) {
			Time.timeScale = 1.0f;
			Destroy (gameObject);
		}
	}
}
