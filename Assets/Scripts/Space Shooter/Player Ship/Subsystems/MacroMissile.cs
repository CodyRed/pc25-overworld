﻿using UnityEngine;
using System.Collections;

public class MacroMissile : Subweapon {

	public float Interval;
	private float maxinterval;
	private Bullet currentbullet;
	private bool firing;

	void Start () {
		maxcooldown = Cooldown;
		maxinterval = Interval;
		firing = false;
	}

	void Update () {
		if (!firing) {
			Cooldown -= 1 * Time.deltaTime;
			if (Cooldown < 0) {
				Cooldown = 0;
			}
		}

		if (firing) {
			InvokeRepeating("Macross", 0, 3.0f);
			Interval -= 1 * Time.deltaTime;
		}

		if ((Interval <= 0) && (firing)) {
			CancelInvoke("Macross");
			firing = false;
			Interval = maxinterval;
		}
	}

	public override void Activate()
	{
		Cooldown = maxcooldown;
		firing = true;
	}

	void Macross()
	{
		currentbullet = Instantiate (MyProjectile, transform.position, Quaternion.identity) as Bullet;
		currentbullet.GetComponent<Bullet> ().DidPlayerShoot ();
		currentbullet.GetComponent<Bullet> ().SetStrength (Damage);
	}
}
