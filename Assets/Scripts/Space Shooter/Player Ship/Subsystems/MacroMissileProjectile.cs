﻿using UnityEngine;
using System.Collections;

public class MacroMissileProjectile : MonoBehaviour {

	private float where;
	private Vector3 pos;
	private Vector3 direction;

	//The Macross Missile can fly in any of the 8 basic directions, determined randomly.
	void Start () {
		where = Random.Range (0, 8);
	}
	
	// Update is called once per frame
	void Update () {
		if (where == 0) {
			direction = (Vector3.forward - Vector3.left)/2;
		} else if (where == 1) {
			direction = Vector3.forward;
		} else if (where == 2) {
			direction = (Vector3.forward - Vector3.right)/2;
		} else if (where == 3) {
			direction = Vector3.right;
		} else if (where == 4) {
			direction = new Vector3 (1, 0, -1);
			direction.Normalize ();
		} else if (where == 5) {
			direction = new Vector3(0, 0, -1);
		} else if (where == 6) {
			direction = new Vector3 (-1, 0, -1);
		} else {
			direction = new Vector3 (-1, 0, 0);
		}

		direction.Normalize ();
		transform.position += direction * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
	}
}
