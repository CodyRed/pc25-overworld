﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public int RandomPower;
	public float Cooldown = 30.0f;
	public Text PowerUpInfo;
	private bool boughtPowerUp;
	private float maxCooldown;
	private bool boughtPassword;

	public AudioClip SpeedSound;
	public AudioClip BuySound;
	public AudioClip ErrorSound;
	public AudioClip ShrinkSound;
	public AudioClip CDSound;
	public AudioClip PowerSound;
	public GameObject PauseMaster;
	public GameObject PasswordScreen;

	public GameObject Patcher;
	public GameObject Tower;
	private GameObject myTower;
	private bool hasTower;

	void Start () {
		Patcher = GameObject.Find ("SShooterMaster");
		PauseMaster = GameObject.Find ("PauseMaster");
		maxCooldown = Cooldown;
		Cooldown = 0.0f;
	}

	void Update () {
		if (Patcher.GetComponent<SShooterSwitches> ().isShoppingEnabled == false) {
			PowerUpInfo.text = "[SHOPPING DISABLED]";
			return;
		}
			RunCooldown ();
			ShopDisplay ();
			if (Input.GetKeyUp (KeyCode.Q)) {
				BuyPowerUp ();
			}
	}

	void BuyPowerUp() {
		if (boughtPowerUp == false) {
			if (RandomPower == 0) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 1000) {
					if (gameObject.GetComponent<PlayerMovement> ().PlayerSpeed <= 20.0f) {
						GetComponent<AudioSource> ().PlayOneShot (SpeedSound);
						gameObject.GetComponent<CashHandler> ().Cash -= 1000;
						gameObject.GetComponent<PlayerMovement> ().PlayerSpeed += 0.75f;
						boughtPowerUp = true;
					} else {
						GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
					}
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} else if (RandomPower == 1) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 4000) {
					if (gameObject.transform.localScale.x >= 0.3f) {
						GetComponent<AudioSource> ().PlayOneShot (ShrinkSound);
						gameObject.GetComponent<CashHandler> ().Cash -= 4000;
						transform.localScale += new Vector3 (-0.1f, -0.1f, -0.1f);
						boughtPowerUp = true;
					} else {
						GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
					}
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} else if (RandomPower == 2) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 5000) {
					GetComponent<AudioSource> ().PlayOneShot (PowerSound);
					gameObject.GetComponent<CashHandler> ().Cash -= 5000;
					gameObject.GetComponent<EquippedWeapon> ().MyWeapon.GetComponent<Weapon> ().Damage += 1;
					boughtPowerUp = true;
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} else if (RandomPower == 3) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 10000) {
					if (boughtPassword == false) {
						boughtPassword = true;
						GetComponent<AudioSource> ().PlayOneShot (BuySound);
						gameObject.GetComponent<CashHandler> ().Cash -= 10000;
						PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
						PasswordScreen.SetActive (true);
						boughtPowerUp = true;
					} else {
					}
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} else if (RandomPower == 4) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 4000) {
					if (gameObject.GetComponent<EquippedWeapon> ().MyWeapon.GetComponent<Weapon> ().Cooldown >= 0.25f) {
						GetComponent<AudioSource> ().PlayOneShot (CDSound);
						gameObject.GetComponent<CashHandler> ().Cash -= 4000;
						gameObject.GetComponent<EquippedWeapon> ().MyWeapon.GetComponent<Weapon> ().Cooldown -= 0.25f;
						boughtPowerUp = true;
					} else {
						GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
					}
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} else if (RandomPower == 5) {
				if (gameObject.GetComponent<CashHandler> ().Cash >= 6000) {
					GetComponent<AudioSource> ().PlayOneShot (PowerSound);
					gameObject.GetComponent<CashHandler> ().Cash -= 6000;
					gameObject.GetComponent<Respawn> ().Lives += 1;
					boughtPowerUp = true;
				} else {
					GetComponent<AudioSource> ().PlayOneShot (ErrorSound);
				}
			} 
		} else if (boughtPowerUp == true) {
			PowerUpInfo.text = "Next item in : " + Cooldown + " seconds.";
		}
	}

	void ShopDisplay() {
		//Speed
		if (boughtPowerUp == true) {
			PowerUpInfo.text = "Thanks for shopping!";
		} else if (RandomPower == 0) {
			PowerUpInfo.text = "Now Available: Speed ($1000)";
		} //Size
		else if (RandomPower == 1) {
			PowerUpInfo.text = "Now Available: Size ($4000)";
		} //Damage
		else if (RandomPower == 2) {
			PowerUpInfo.text = "Now Available: Damage ($5000)";
		} //Password
		else if (RandomPower == 3) {
			if (boughtPassword == true) {
				PowerUpInfo.text = "[SYSTEM REPAIR]: Sold out!!";
			} else {
				PowerUpInfo.text = "Now Available: [SYSTEM REPAIR] ($10000)";
			}
		} //Attack Speed
		else if (RandomPower == 4) {
			PowerUpInfo.text = "Now Available: Attack Speed ($4000)";
		} //Life
		else if (RandomPower == 5) {
			PowerUpInfo.text = "Now Available: Life ($6000)";
		} //Tower
	}

	void RunCooldown() {
		if (Cooldown > 0.0f) {
			Cooldown -= 1 * Time.deltaTime;
		} else if (Cooldown <= 0.0f) {
			boughtPowerUp = false;
			Cooldown = maxCooldown;
			RandomPower = Random.Range (0, 5);
		}
	}
}
