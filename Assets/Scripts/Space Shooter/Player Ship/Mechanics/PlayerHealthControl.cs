﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Created with help from https://www.youtube.com/watch?v=fkUpXpD5PRs
public class PlayerHealthControl : MonoBehaviour {

	public Image HealthBar;
	public GameObject RestartMenu;
	public AudioSource GameOverMusic;
	private float maxhealth;

	void Start () {
		RestartMenu.SetActive (false);
		Time.timeScale = 1.0f;
		maxhealth = gameObject.GetComponent<Health> ().MyHealth;
	}

	void Update () {
		DrawHealth ();
		if (gameObject.GetComponent<Health> ().MyHealth <= 0) {
			RestartMenu.SetActive (true);
			gameObject.GetComponent<AudioSource>().Stop();
			Time.timeScale = 0.0f;
		}
	}

	void DrawHealth()
	{
		HealthBar.rectTransform.localScale = new Vector3
			(gameObject.GetComponent<Health> ().MyHealth/maxhealth , 
			 HealthBar.transform.localScale.y,
			 HealthBar.transform.localScale.z);
	}

	public void Restart()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public float GetMaxHealth()
	{
		return maxhealth;
	}
}
