﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CashHandler : MonoBehaviour {

	public int Cash;
	public Text CashDisplay;

	void Update() {
		CashDisplay.text = "Cash: " + Cash.ToString ();
	}

	public void AddCash(int reward)
	{
		Cash += reward;
	}
}
