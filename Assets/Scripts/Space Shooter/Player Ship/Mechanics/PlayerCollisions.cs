using UnityEngine;
using System.Collections;

public class PlayerCollisions : MonoBehaviour {

	public GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("SShooterMaster");
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<VictoryChecker> () != null) {
			return;
		}

		if (other.gameObject.GetComponent<Enemy> () != null) {
			//And I'm not invulnerable
			if (gameObject.GetComponent<Respawn> ().InvulTime <= 0) {
				gameObject.GetComponent<Health> ().MyHealth -= 1;
			}
		}

		if (Patcher != null) {
			if (Patcher.GetComponent<SShooterSwitches> ().isColliding == true) {
				if (other.tag == "Wall") {
					//Patcher only disables collision with wall
					if (Patcher.GetComponent<SShooterSwitches> ().isColliding == true) {
						if (gameObject.GetComponent<Respawn> ().InvulTime <= 0) {
							gameObject.GetComponent<Health> ().MyHealth = 0;
						}
					} else {
						return;
					}
				}	
			}
		}	
	}
}
