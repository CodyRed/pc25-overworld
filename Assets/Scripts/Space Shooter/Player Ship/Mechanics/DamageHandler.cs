﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour {

	public void TakeDamage (float damage) {
			gameObject.GetComponent<Health>().MyHealth -= damage;
	}
}