﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CashDisplay : MonoBehaviour {
	
	public Text CashUi;

	void Update () {
		CashUi.text = "$" + gameObject.GetComponent<CashHandler> ().Cash;
	}
}
