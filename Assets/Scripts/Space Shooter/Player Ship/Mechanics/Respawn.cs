﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Respawn : MonoBehaviour {

	public float Lives;
	public float InvulTime;
	private Color myColor;

	private bool playOnce;
	private float maxInvulTime;
	private float initialSpeed;

	public GameObject Defeat;
	public GameObject PauseMaster;

	public Text LifeText;
	public GameObject Explosion;
	public AudioClip DeathSound;

	void Awake () {
		PauseMaster = GameObject.Find ("PauseMaster");
		maxInvulTime = InvulTime;
	}

	void Start() {
		initialSpeed = gameObject.GetComponent<PlayerMovement> ().PlayerSpeed;
		myColor = gameObject.GetComponent<Renderer> ().material.color;
		InvulTime = 0;
	}

	void Update () {
		LifeText.text = "Lives: " + Lives;

		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (InvulTime > 0) {
				InvulTime -= 1 * Time.deltaTime;
				gameObject.GetComponent<Renderer> ().material.color = Color.white;
				gameObject.GetComponent<Health> ().enabled = false;

			} else if (InvulTime <= 0) {
				InvulTime = 0;
				gameObject.GetComponent<Health> ().enabled = true;
				gameObject.GetComponent<Renderer> ().material.color = myColor;
			}
		}

		if (gameObject.GetComponent<Health> ().MyHealth <= 0) {
			if (Lives > 0) {
				RespawnShip ();
			} else {
				if (playOnce == false) {
					GetComponent<AudioSource> ().PlayOneShot (DeathSound);
					Instantiate (Explosion, transform.position, Quaternion.identity);
				}
				playOnce = true;
				PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
				Defeat.SetActive (true);
				}
			}
		}

	void RespawnShip() {
		GetComponent<AudioSource> ().PlayOneShot (DeathSound);
		Instantiate (Explosion, transform.position, Quaternion.identity);
		gameObject.GetComponent<Health> ().MyHealth = 1;
		Lives -= 1;
		gameObject.transform.position = new Vector3 (0.0f, 0.0f, 0.0f);
		InvulTime = maxInvulTime;
	}
}
