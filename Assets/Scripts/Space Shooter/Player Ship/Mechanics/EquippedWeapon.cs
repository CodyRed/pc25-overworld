﻿using UnityEngine;
using System.Collections;

public class EquippedWeapon : MonoBehaviour {

	public Weapon MyWeapon;
	public AudioClip PewPew;
	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		MyWeapon.transform.parent = gameObject.transform;
		MyWeapon.transform.position = gameObject.transform.position;
	}

	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (Input.GetKey (KeyCode.Space)) {
				if (MyWeapon.GetComponent<Weapon> ().Cooldown <= 0) {
					GetComponent<AudioSource> ().PlayOneShot (PewPew);
					if (gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge
					   >= MyWeapon.GetComponent<Weapon> ().GeneratorDrain) {
						MyWeapon.GetComponent<Weapon> ().Fire ();
						gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
						-= MyWeapon.GetComponent<Weapon> ().GeneratorDrain;
					}
				}
			}
		}
	}
}
