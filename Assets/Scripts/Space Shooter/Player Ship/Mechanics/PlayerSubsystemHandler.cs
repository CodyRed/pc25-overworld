﻿using UnityEngine;
using System.Collections;

public class PlayerSubsystemHandler : MonoBehaviour {

	public Subsystem SSystem1;
	public Subsystem SSystem2;
	
	void Start () {
		if (SSystem1.GetComponent<Subweapon> () != null) {
			SSystem1.transform.parent = gameObject.transform;
			SSystem1.transform.position = gameObject.transform.position;
		}
		if (SSystem2.GetComponent<Subweapon> () != null) {
			SSystem2.transform.parent = gameObject.transform;
			SSystem2.transform.position = gameObject.transform.position;
		}
	}

	//The trouble is that Subsystems are Gameobjects.
	//I'm having trouble linking those that affects hp, shields, etc.
	//So I'll pass execution of passive-effect subsystems to this handler.
	void Update () {
		RunPassives ();
		if (Input.GetKeyUp (KeyCode.Q)) 
		{
			//For passive effect subsystems.
			if (SSystem1.GetComponent<RepairBot>() != null)
			{
				if ((gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
			   	>= SSystem1.ActivationCost) && (SSystem1.Activated == 0))
				{
					Debug.Log ("Activating " + SSystem1.Name + " subsystem.");
					if (gameObject.GetComponent<Health>().MyHealth < (gameObject.GetComponent<Health>().GetMaxHealth()))
					{
					gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
						-= SSystem1.ActivationCost;
					SSystem1.Activated = 1;
					}
					else
					{
						Debug.Log ("No hull repairs needed.");
					}
				}
				else if (SSystem1.Activated == 1)
				{
					SSystem1.Activated = 0;
				}
			}
			//For subweapon type subsystems.
			if ((gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
			     >= SSystem1.ActivationCost))
			{
				gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
					-= SSystem1.ActivationCost;
				SSystem1.Activate();
			}
		}

		if (Input.GetKeyUp (KeyCode.E)) {
			if (SSystem2.GetComponent<RepairBot>() != null)
			{
				if ((gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
				     >= SSystem2.ActivationCost) && (SSystem2.Activated == 0))
				{
					Debug.Log ("Activating " + SSystem2.Name + " subsystem.");
					if (gameObject.GetComponent<Health>().MyHealth < (gameObject.GetComponent<Health>().GetMaxHealth()))
					{
						gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
							-= SSystem2.ActivationCost;
						SSystem2.Activated = 1;
					}
					else
					{
						Debug.Log ("No hull repairs needed.");
					}
				}
				else if (SSystem2.Activated == 1)
				{
					SSystem2.Activated = 0;
				}
			}
			//For subweapon type subsystems.
			else
			{
				if ((gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
				     >= SSystem2.ActivationCost))
				{
					gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
						-= SSystem2.ActivationCost;
					SSystem2.Activate();
				}
			}
		}
	}

	void RunPassives()
	{
		if (SSystem1.Activated == 1) {
			if (gameObject.GetComponent<Health>().MyHealth < gameObject.GetComponent<Health>().GetMaxHealth())
			{
				Debug.Log ("Repairing hull integrity.");
				gameObject.GetComponent<Health>().MyHealth += SSystem1.GetComponent<RepairBot>().RegenerationRate * Time.deltaTime;
				gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
					-= SSystem1.GeneratorDrain * Time.deltaTime;
			}
			else
			{
				Debug.Log ("Deactivating due to full health.");
				SSystem1.Activated = 0;
			}
		}
		
		//If I cannot pay the energy cost, shut down the subsystem.
		if ((SSystem1.Activated == 1) && 
		    (gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
		 < SSystem1.ActivationCost)) {
			Debug.Log ("Subsystem shutdown imminent.");
			SSystem1.Activated = 0;
		}

		if (SSystem2.Activated == 1) {
			if (gameObject.GetComponent<Health>().MyHealth < gameObject.GetComponent<Health>().GetMaxHealth())
			{
				Debug.Log ("Repairing hull integrity.");
				gameObject.GetComponent<Health>().MyHealth += SSystem1.GetComponent<RepairBot>().RegenerationRate * Time.deltaTime;
				gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
					-= SSystem2.GeneratorDrain * Time.deltaTime;
			}
			else
			{
				Debug.Log ("Deactivating due to full health.");
				SSystem2.Activated = 0;
			}
		}

		if ((SSystem2.Activated == 1) && 
		    (gameObject.GetComponent<PlayerEnergyControl> ().MyBattery.GetComponent<Battery> ().CurrentCharge 
		 < SSystem2.ActivationCost)) {
			Debug.Log ("Subsystem shutdown imminent.");
			SSystem1.Activated = 2;
		}
	}
}
