﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Created with help from https://www.youtube.com/watch?v=fkUpXpD5PRs
public class PlayerShieldsControl : MonoBehaviour {

	public Shields MyShield;
	public Image ShieldsBar;
	private float maxshield;

	void Start () {
		maxshield = MyShield.MaxShields;
		MyShield.transform.parent = gameObject.transform;
		MyShield.transform.position = gameObject.transform.position;
	}

	void Update () {
		DrawShields ();
		if (MyShield.ShieldHealth >= maxshield) {
			MyShield.ShieldHealth = maxshield;
		}

		if (MyShield.ShieldHealth < 0) {
			MyShield.ShieldHealth = 0;
		}
		
		if (MyShield.ShieldHealth < maxshield) {
			MyShield.ShieldHealth += MyShield.RegenerationRate * Time.deltaTime;
			gameObject.GetComponent<PlayerEnergyControl>().MyBattery.GetComponent<Battery>().CurrentCharge 
				-= MyShield.GeneratorDrain * Time.deltaTime;
		}
	}
	
	void DrawShields()
	{
		ShieldsBar.rectTransform.localScale = new Vector3
			(MyShield.ShieldHealth/maxshield , 
			 ShieldsBar.transform.localScale.y,
			 ShieldsBar.transform.localScale.z);
	}
}
