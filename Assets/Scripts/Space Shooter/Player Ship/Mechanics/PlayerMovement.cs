﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	
	public static Vector3 Player;
	public Camera MyCamera;
	public float PlayerSpeed;

	public float MinX;
	public float MaxX;
	public float MinZ;
	public float MaxZ;

	public GameObject Patcher;
	private GameObject PauseMaster;

	public bool isNotNative;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
		Patcher = GameObject.Find ("SShooterMaster");
	}

	void Update () {
		//Bypass checks
		if (isNotNative == true) {
			Player = this.transform.position;
			float HorizontalValue = Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime;
			this.transform.Translate (Vector3.right * HorizontalValue);

			float VerticalValue = Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime;
			this.transform.Translate (Vector3.forward * VerticalValue);

			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, MinX, MaxX), 0.0f, 
				Mathf.Clamp (transform.position.z, MinZ, MaxZ));
			
			return;
		}
		
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (Patcher != null) {
				if (Patcher.GetComponent<SShooterSwitches> ().CanMove == false) {
					return;
				}
			}
			Player = this.transform.position;
			float HorizontalValue = Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime;
			this.transform.Translate (Vector3.right * HorizontalValue);

			float VerticalValue = Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime;
			this.transform.Translate (Vector3.forward * VerticalValue);

			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, MinX, MaxX), 0.0f, 
				Mathf.Clamp (transform.position.z, MinZ, MaxZ));
		} else {
			return;
		}
	}
}