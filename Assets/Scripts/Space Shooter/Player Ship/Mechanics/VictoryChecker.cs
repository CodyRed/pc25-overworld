﻿using UnityEngine;
using System.Collections;

public class VictoryChecker : MonoBehaviour {

	public GameObject Victory;
	public GameObject PauseMaster;
	public GameObject Patcher;

	void Start()
	{
		Patcher = GameObject.Find ("SShooterMaster");
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<PlayerMovement>() != null)
		{
			Patcher.GetComponent<SShooterSwitches> ().isShooterComplete = true;
			PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			other.gameObject.GetComponent<AudioSource>().Stop();
			gameObject.GetComponent<AudioSource>().Play();
			Victory.SetActive (true);
		}
	}
}
