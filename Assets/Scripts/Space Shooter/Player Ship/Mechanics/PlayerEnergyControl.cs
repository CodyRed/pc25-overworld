﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEnergyControl : MonoBehaviour {

	public Image BatteryBar;
	public Battery MyBattery;
	public Generator MyGenerator;

	void Start () {
		MyBattery.GetComponent<Battery> ().CurrentCharge = MyBattery.GetComponent<Battery> ().MaxCapacity / 4;
	}

	void Update () {
		if (MyBattery.GetComponent<Battery> ().CurrentCharge > MyBattery.GetComponent<Battery> ().MaxCapacity) {
			MyBattery.GetComponent<Battery> ().CurrentCharge = MyBattery.GetComponent<Battery> ().MaxCapacity;
		}

		if (MyBattery.GetComponent<Battery> ().CurrentCharge < MyBattery.GetComponent<Battery> ().MaxCapacity) {
			MyBattery.GetComponent<Battery> ().CurrentCharge += MyGenerator.GetComponent<Generator>().RechargeRate 
				* Time.deltaTime;
		}

		if (BatteryBar != null) {
			BatteryBar.rectTransform.localScale = new Vector3
			(MyBattery.GetComponent<Battery> ().CurrentCharge / MyBattery.GetComponent<Battery> ().MaxCapacity, 
			 BatteryBar.transform.localScale.y,
			 BatteryBar.transform.localScale.z);
		}
	}
}
