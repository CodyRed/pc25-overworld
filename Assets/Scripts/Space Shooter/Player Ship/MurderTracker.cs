﻿using UnityEngine;
using System.Collections;

public class MurderTracker : MonoBehaviour {

	public int MurderCount;
	private bool runOnce;
	public GameObject BonusScreen;
	public GameObject PauseMaster;

	// Use this for initialization
	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
	}
	
	// Update is called once per frame
	void Update () {
		if (runOnce == false) {
			if (MurderCount >= 300) {
				runOnce = true;
				BonusScreen.SetActive (true);
				PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			}
		}
	}

	public void AddMurder() {
		MurderCount++;
	}
}
