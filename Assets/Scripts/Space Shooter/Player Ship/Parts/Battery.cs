﻿using UnityEngine;
using System.Collections;

public class Battery : MonoBehaviour {
	public string Name;
	public float MaxCapacity;
	public float CurrentCharge;
}

