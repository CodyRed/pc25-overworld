﻿using UnityEngine;
using System.Collections;

public class Shields : MonoBehaviour {
	public string Name;
	public float MaxShields;
	public float ShieldHealth;
	public float RegenerationRate;
	public float GeneratorDrain;

	void Start()
	{
		ShieldHealth = MaxShields;
	}
}
