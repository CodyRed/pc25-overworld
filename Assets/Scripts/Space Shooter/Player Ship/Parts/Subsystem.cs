﻿using UnityEngine;
using System.Collections;

public class Subsystem : MonoBehaviour {

	//Some subsystems constantly consume power while on. Some use energy as a one-time thing. Some do both.
	public string Name;
	public float GeneratorDrain;
	public float ActivationCost;
	public int Activated;


	void Start () {
		Activated = 0;
	}

	void Update () {
	
	}

	public virtual void Activate()
	{

	}
}
