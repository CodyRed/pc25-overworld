﻿using UnityEngine;
using System.Collections;

public class SShooterSwitches : MonoBehaviour {

	public bool isColliding = true;
	public bool InfiniteHealth = false;
	public bool CanMove = false;
	public bool isShoppingEnabled = false;
	public bool isShooterComplete = false;
    private static bool createdsp = false;
    public static bool ssdone = false;

    void Awake() {
        //DontDestroyOnLoad (gameObject);
        if (!ssdone)
        {
            // this is the first instance - make it persist
            DontDestroyOnLoad(this.gameObject);
            ssdone = true;
        }
        else {
            // this must be a duplicate from a scene reload - DESTROY!
            Destroy(this.gameObject);
        }
    }

	void Start () {
	
	}

    void Update()
    {
        if (isShooterComplete)
        {
            ssdone = true;
        }
    }

    public void EnableMovement() {
		CanMove = true;
	}

	public void DisableMovement() {
		CanMove = false;
	}

	public void EnableCollision() {
		isColliding = true;
	}

	public void DisableCollision() {
		isColliding = false;
	}

	public void EnableInfiniteHealth() {
		InfiniteHealth = true;
	}

	public void DisableInfiniteHealth() {
		InfiniteHealth = false;
	}

	public void EnableShopping() {
		isShoppingEnabled = true;
	}

	public void DisableShopping() {
		isShoppingEnabled = false;
	}
}
