﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float Speed;
	public float TimedLifeDuration;
	private Vector3 target;
	public bool AirMode;
	public float strength;
	public GameObject Explosion;
	private bool playershot;

	void Start(){

	}

	void Update() {
		TimedLifeDuration -= 1 * Time.deltaTime;
		if (TimedLifeDuration <= 0) {
			Destroy (gameObject);
		}
	}

	public void DidPlayerShoot()
	{
		playershot = true;
	}

	public void SetTarget(Vector3 newtarget)
	{
		target = newtarget;
	}

	public void SetStrength(int newstr)
	{
		strength = newstr;
	}

	void OnTriggerEnter(Collider other)
	{
		//Check for the health components. Logic says we should really only just use one,
		//but the nature of the game? Probably safer to keep it this way.
		if (!playershot) {
			if (other.gameObject.GetComponent<DamageHandler> () != null) {
				if (other.gameObject.GetComponent<Respawn> ().InvulTime <= 0) {
					other.gameObject.GetComponent<DamageHandler> ().TakeDamage (1);
					Instantiate (Explosion, transform.position, Quaternion.identity);
					Destroy (gameObject);
				}
			}
		} else if (playershot) {
			if (other.gameObject.GetComponent<Enemy> () != null) {
				other.gameObject.GetComponent<Health> ().MyHealth -= strength;
				Instantiate (Explosion, transform.position, Quaternion.identity);
				Destroy(gameObject);
			}

			if (other.gameObject.GetComponent<HitPoints> () != null) {
				other.gameObject.GetComponent<HitPoints>().Health -= strength;
				Instantiate (Explosion, transform.position, Quaternion.identity);
				Destroy(gameObject);
			}
		}
	}
}
