﻿using UnityEngine;
using System.Collections;

public class Straight : MonoBehaviour {

	void Start () {
	
	}

	void Update () {
		if (GetComponent<Bullet>().AirMode == false) {
			Vector3 direction = Vector3.forward - Vector3.back;
			direction.Normalize ();
			transform.position += direction * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
		} else {
			Debug.Log("I am firing in Airmode!");
			Vector3 direction = Vector3.right - Vector3.left;
			direction.Normalize ();
			transform.position += direction * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
		}
	}
}
