﻿using UnityEngine;
using System.Collections;

public class LeftDiagonal : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 direction = (Vector3.forward - Vector3.left) / 2;
		direction.Normalize ();
		transform.position += direction * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
	}
}
