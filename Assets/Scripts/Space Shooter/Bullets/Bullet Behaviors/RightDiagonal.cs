﻿using UnityEngine;
using System.Collections;

public class RightDiagonal : MonoBehaviour {

	void Start () {
	
	}

	void Update () {
		Vector3 direction = (Vector3.forward - Vector3.right) / 2;
		direction.Normalize ();
		transform.position += direction * gameObject.GetComponent<Bullet> ().Speed * Time.deltaTime;
	}
}
