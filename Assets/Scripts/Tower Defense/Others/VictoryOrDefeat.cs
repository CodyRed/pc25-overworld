﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class VictoryOrDefeat : MonoBehaviour {

	public GameObject LifeManager;
	public GameObject SpawnManager;
	public GameObject DefeatMenu;
	public GameObject VictoryMenu;

	public List<GameObject> MonsterCount = new List<GameObject> ();

	void Update () {
		if (LifeManager.GetComponent<LifeManager> ().Lives <= 0) {
			MonsterCount.AddRange (GameObject.FindGameObjectsWithTag ("Enemy"));
			foreach (GameObject victim in MonsterCount) {
				Destroy (victim);
			}
			DefeatMenu.SetActive (true);
		}

		if (SpawnManager.GetComponent<TDSpawner> ().isDone == true) {
			VictoryMenu.SetActive (true);
		}

	}

	public void Restart() {
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Escape() {
		Application.LoadLevel ("Overworld");
	}
}
