﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifeManager : MonoBehaviour {

	public int Lives = 30;
	public Text LivesText;
	public GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("TDMaster");
	}

	void Update () {
		if (Patcher != null) {
			if (Patcher.GetComponent<TDSwitches> ().InfiniteLives == true) {
				Lives = 9999;
			}
		}

		if (Lives <= 0) {
			Debug.Log ("You lose");
		}

		LivesText.text = Lives.ToString();
	}
}
