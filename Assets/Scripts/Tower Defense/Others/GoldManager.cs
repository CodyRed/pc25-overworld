﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoldManager : MonoBehaviour {
	public int MyGold = 300;
	public Text GoldParser;
	public GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("TDMaster");
	}

	void Update () {
		if (Patcher != null) {
			if (Patcher.GetComponent<TDSwitches> ().InfiniteGold == true) {
				MyGold = 9999;
			}
		}
		GoldParser.text = MyGold.ToString ();
	}

}
