﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TowerTooltip : MonoBehaviour {

	public Text MyText;
	public string Info;

	public void DisplayInfo() {
		MyText.text = Info;
	}

	public void ClearInfo() {
		MyText.text = "";
	}
}