﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPanel : MonoBehaviour
{
	public GameObject CurrentTarget;
	public Text MyText;

	void Start ()
	{
		CurrentTarget = GameObject.Find ("Build Manager");
	}

	void Update ()
	{
		CheckDisplay ();
	}

	void CheckDisplay ()
	{
		if (CurrentTarget != null) {
			if (CurrentTarget.GetComponent<TowerAttack> () != null) {
				MyText.text = CurrentTarget.name + " | Damage: " +
					CurrentTarget.GetComponent<TowerAttack> ().MinimumDamage + "-" +
					CurrentTarget.GetComponent<TowerAttack> ().MaximumDamage +
				" | Upgrade Cost: " + CurrentTarget.GetComponent<Upgrade> ().Cost;
			} else if (CurrentTarget.GetComponent<TDPathFollow> () != null) {
				MyText.text = CurrentTarget.name + " | Health " + CurrentTarget.GetComponent<HitPoints> ().Health
				+ "/" + CurrentTarget.GetComponent<HitPoints> ().GetMaxHealth () +
				" | Speed: " + CurrentTarget.GetComponent<TDPathFollow> ().speed;
			} else if (CurrentTarget.tag == "PileORox") {
				MyText.text = CurrentTarget.name + " | Health " + CurrentTarget.GetComponent<HitPoints> ().Health +
				"/ " + CurrentTarget.GetComponent<HitPoints> ().GetMaxHealth () + " | Huh.";
			}
		} else {
			MyText.text = "";
		}
	}
}
