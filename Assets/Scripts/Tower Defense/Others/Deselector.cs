﻿using UnityEngine;
using System.Collections;

public class Deselector : MonoBehaviour {

	public GameObject BuildTower1;
	public GameObject BuildTower2;

	void Start () {
		BuildTower1 = GameObject.Find ("Build Arrow");
		BuildTower2 = GameObject.Find ("Build Cannon");
	}

	void OnMouseUpAsButton(){
		Debug.Log ("Deselecting.");
		BuildTower1.SetActive (false);
		BuildTower2.SetActive (false);
	}
}
