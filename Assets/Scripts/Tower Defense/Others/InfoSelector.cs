﻿using UnityEngine;
using System.Collections;

public class InfoSelector : MonoBehaviour {

	public GameObject InfoMgr;

	void Awake() {
		InfoMgr = GameObject.Find ("InfoManager");
	}

	void OnMouseUpAsButton(){
		InfoMgr.GetComponent<InfoPanel> ().CurrentTarget = gameObject;
	}
}
