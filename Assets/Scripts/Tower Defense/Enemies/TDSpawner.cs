﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TDSpawner : MonoBehaviour {

	public float Interval;
	public float TimeBetweenWaves;
	public List<MonsterWave> Waves = new List<MonsterWave> ();
	public float spawnTimer;
	public float spawnQuantity;
	public int iterator;
	public bool isDone;
	public Text WaveInfoText;
	public GameObject DescriptionPanel;
	public GameObject VictoryScreen;
	public Text WaveDescription;

	public string Greeba;
	public string LilGummy;
	public string MidGummy;
	public string BigGummy;
	public string EpicGummy;
	public string BakaShinobi;
	public string SugoiShinobi;
	public string ToxicFloof;
	public string Burster;
	public string Torstudo;
	public string MonsterDesc;

	public List<GameObject> MonsterCount = new List<GameObject> ();
	public AudioClip WaveStart;

	public bool FinalWave;
	public GameObject PauseMaster;
	private Vector3 descPanelPosition;
	private GameObject currentMonster;
	public GameObject bossMob;
	public GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("TDMaster");
		PauseMaster = GameObject.Find ("PauseMaster");
		descPanelPosition = DescriptionPanel.transform.position;
	}

	void Update ()
	{
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		Countdown ();

		if (FinalWave == true) {
			bossMob = GameObject.Find ("Epic Gummy");
		}

		if (FinalWave == false) {
			if (isDone == false) {
				if (Interval <= 0) {
					if (spawnTimer <= 0) {
						Spawn (Waves [iterator]);
					}
				}
			} else {
				if (MonsterCount.Count <= 0) {
					VictoryScreen.gameObject.SetActive (true);
				}
			}
		} else if ((FinalWave == true) && (isDone == false)) {
			if (bossMob != null) {
				if (bossMob.GetComponent<HitPoints> ().Health <= 0) {
					MonsterCount.AddRange (GameObject.FindGameObjectsWithTag ("Enemy"));
					foreach (GameObject victim in MonsterCount) {
						Destroy (victim);
					}
					Patcher.GetComponent<TDSwitches> ().isTdComplete = true;
					isDone = true;
				}
			}
		}
	}

	void Spawn (MonsterWave currentwave)
	{
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		currentMonster = Instantiate (currentwave.MyMonster, transform.position, Quaternion.identity) as GameObject;
		if (FinalWave == true) {
			bossMob = currentMonster;
		}
		currentMonster.name = currentwave.MyMonster.name;
		spawnQuantity++;
		spawnTimer = currentwave.TimeBetweenSpawns;
		CheckForNextWave (currentwave.Quantity);
	}

	void Countdown ()
	{
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		if (Interval > 0) {
			string MonsterName = (Waves [iterator].MyMonster.name);
			if (MonsterName == "Lil' Greeba") {
				MonsterDesc = Greeba;
			} else if (MonsterName == "Lil' Gummy") {
				MonsterDesc = LilGummy;
			} else if (MonsterName == "Medium Gummy") {
				MonsterDesc = MidGummy;
			} else if (MonsterName == "Huge Gummy") {
				MonsterDesc = BigGummy;
			} else if (MonsterName == "Epic Gummy") {
				MonsterDesc = EpicGummy;
			} else if (MonsterName == "Torstudo") {
				MonsterDesc = Torstudo;
			} else if (MonsterName == "Burster") { 
				MonsterDesc = Burster;
			} else if (MonsterName == "Toxic Floof") {
				MonsterDesc = ToxicFloof;
			} else if (MonsterName == "Baka Shinobi") {
				MonsterDesc = BakaShinobi;
			} else {
				MonsterDesc = SugoiShinobi;
			}
			WaveInfoText.text = "Next wave in: " + Interval.ToString("F1");
			Interval -= 1 * Time.deltaTime;
			DescriptionPanel.transform.position = descPanelPosition;
			WaveDescription.text = "Next wave: " + System.Environment.NewLine +Waves [iterator].MyMonster.name + " x " + Waves [iterator].Quantity 
				+ System.Environment.NewLine + MonsterDesc;
		}

		if (Interval < 0) {
			GetComponent<AudioSource> ().PlayOneShot (WaveStart);
			DescriptionPanel.transform.position = new Vector3 (0.0f, -2000.0f, 0.0f);
			WaveDescription.text = " ";
			WaveInfoText.text = " ";
			Interval = 0;
		}

		if (spawnTimer > 0) {
			spawnTimer -= 1 * Time.deltaTime;
		}

		if (spawnTimer < 0) {
			spawnTimer = 0;
		}
	}
		
	void CheckForNextWave (int goal)
	{
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		if (iterator == (Waves.Count - 1)) {
			FinalWave = true;
			return;
		}
			
		if (spawnQuantity == goal) {
			Interval = TimeBetweenWaves;
			iterator++;
			spawnQuantity = 0;
		}
	}
}
