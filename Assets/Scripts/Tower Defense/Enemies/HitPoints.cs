﻿using UnityEngine;
using System.Collections;

public class HitPoints : MonoBehaviour {

    public float Health;
    private float maxhealth;
	private GameObject newSpawn;
	public GameObject manager;
	public bool isCrossover;
	public GameObject DeathSound;

    void Start()
    {
        maxhealth = Health;
		if (isCrossover == false) {
			manager = GameObject.FindGameObjectWithTag ("GoldManager");
		}
    }

    void Update()
    {
        if (Health > maxhealth)
        {
            Health = maxhealth;
        }

        if (Health <= 0)
        {
			if (DeathSound != null) {
				Instantiate (DeathSound, transform.position, Quaternion.identity);
			}

			if (gameObject.tag == "PileORox") {
				return;
			}
	
			if (manager != null) {
				manager.GetComponent<GoldManager> ().MyGold += gameObject.GetComponent<GoldHandler> ().Gold;
			}

			if (gameObject.GetComponent<BadSeed>() != null) 
			{
				int Destination = gameObject.GetComponent<TDPathFollow>().curPathIndex;
				newSpawn = Instantiate(gameObject.GetComponent<BadSeed>().MySpawn, transform.position, Quaternion.identity) as GameObject;
				newSpawn.GetComponent<TDPathFollow>().curPathIndex = Destination;
				newSpawn.name = gameObject.GetComponent<BadSeed>().MySpawn.name;
			}
            Destroy(gameObject);
        }
    }

	public float GetMaxHealth()
	{
		return maxhealth;
	}
}
