﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

    public float Health;
    private float maxhealth;
	private GameObject player;

    // Use this for initialization
    void Start()
    {
		player = GameObject.FindGameObjectWithTag ("Player");
        maxhealth = Health;
    }

    // Update is called once per frame
    void Update()
    {
		//Prevent weird decimal health that comes out from time to time
		//gameObject.GetComponent<EnemyHealth>().Health = Mathf.Round (gameObject.GetComponent<EnemyHealth>().Health);
        if (Health > maxhealth)
        {
            Health = maxhealth;
        }

        if (Health <= 0)
        {
			player.GetComponent<GoldHandler>().Gold += gameObject.GetComponent<Bounty>().GoldDrop;
            Destroy(gameObject);
        }
    }

	public float GetMaxHealth()
	{
		return maxhealth;
	}
}
