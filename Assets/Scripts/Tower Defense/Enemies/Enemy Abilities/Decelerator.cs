﻿using UnityEngine;
using System.Collections;

public class Decelerator : MonoBehaviour {

	public float Interval;
	public float Decrement;
	public float MinimumSpeed;

	private float cooldown;

	void Start () {
		cooldown = Interval;
	}

	void Update () {
		Interval -= 1 * Time.deltaTime;

		if (Interval <= 0) {
			Interval = cooldown;
			if (gameObject.GetComponent<TDPathFollow> ().speed > MinimumSpeed) {
				gameObject.GetComponent<TDPathFollow> ().speed -= Decrement;
			} else {
				gameObject.GetComponent<TDPathFollow> ().speed = MinimumSpeed;
			}
		}
	}
}
