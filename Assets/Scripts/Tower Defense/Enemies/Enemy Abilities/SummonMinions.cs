﻿using UnityEngine;
using System.Collections;

public class SummonMinions : MonoBehaviour {

	public GameObject Minion;
	public int Quantity;
	public float Cooldown;
	public int Threshold;
	public float SpawnDelay;

	private float maxDelay;
	private int qtyCounter;
	private float maxCooldown;
	private GameObject mySpawn;

	void Start () {
		maxCooldown = Cooldown;
		maxDelay = SpawnDelay;
		SpawnDelay = 0;
	}

	void Update () {
		Cooldown -= 1 * Time.deltaTime;
		SpawnDelay -= 1 * Time.deltaTime;

		if (Cooldown <= 0) {
			Cooldown = 0;
			if (gameObject.GetComponent<TDPathFollow> ().curPathIndex < Threshold) {
				SpawnMinion ();
			} 
		}
	}

	void SpawnMinion() {
		if (qtyCounter < Quantity) {
			if (SpawnDelay <= 0) {
				SpawnDelay = maxDelay;
				int Destination = gameObject.GetComponent<TDPathFollow> ().curPathIndex;
				mySpawn = Instantiate (Minion, transform.position, Quaternion.identity) as GameObject;
				mySpawn.GetComponent<TDPathFollow> ().curPathIndex = Destination;
				mySpawn.name = Minion.name;
				qtyCounter++;
			}
		} else {
			qtyCounter = 0;
			Cooldown = maxCooldown;
		}
	}
}