﻿using UnityEngine;
using System.Collections;

public class Accelerator : MonoBehaviour {

	public float Interval;
	public float Increment;
	public float MaximumSpeed;

	private float cooldown;

	void Start () {
		cooldown = Interval;
	}

	void Update () {
		Interval -= 1 * Time.deltaTime;

		if (Interval <= 0) {
			Interval = cooldown;
			if (gameObject.GetComponent<TDPathFollow> ().speed < MaximumSpeed) {
				gameObject.GetComponent<TDPathFollow> ().speed += Increment;
			} else {
				gameObject.GetComponent<TDPathFollow> ().speed = MaximumSpeed;
			}
		}
	}
}
