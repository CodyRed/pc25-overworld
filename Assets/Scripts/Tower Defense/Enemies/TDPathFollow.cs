using UnityEngine;
using System.Collections;

//Follows a set of points determined by Path.cs (manually fill out the indices)
//Adapted from Artinte.

public class TDPathFollow : MonoBehaviour 
{
    public GameObject path;
    public float speed = 5.0f;

    private float curSpeed;
    public int curPathIndex;
    private Vector3 targetPoint;

    Vector3 velocity;

	public GameObject PauseMaster;

	void Start () 
    {
		PauseMaster = GameObject.Find ("PauseMaster");
		path = GameObject.FindGameObjectWithTag ("MonsterPath");
	}

	void Update () 
    {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

        curSpeed = speed * Time.deltaTime;
        targetPoint = path.GetComponent<TDPath>().GetPoint(curPathIndex);

		if (Vector3.Distance (transform.position, targetPoint) <= path.GetComponent<TDPath>().Radius) {
			if (path.GetComponent<TDPath>().GetPoint(curPathIndex+1) != null)
			{
				curPathIndex++;
				//Yes, this will go out of bounds, but the life trigger will kill them anyway.
			}
		}
        velocity += Steer(targetPoint);
        transform.position += velocity;
	}

    public Vector3 Steer(Vector3 target, bool bFinalPoint = false)
    {
        Vector3 desiredVelocity = (target - transform.position);
        float dist = desiredVelocity.magnitude;
        desiredVelocity.Normalize();
        if (bFinalPoint && dist < 10.0f) {
			desiredVelocity *= (curSpeed * (dist / 10.0f));
		} else {
			desiredVelocity *= curSpeed;
		}
        Vector3 steeringForce = desiredVelocity - velocity;
		return steeringForce;
    }
}