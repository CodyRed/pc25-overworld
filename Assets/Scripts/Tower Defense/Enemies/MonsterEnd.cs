﻿using UnityEngine;
using System.Collections;

public class MonsterEnd : MonoBehaviour {

	public GameObject LifeHandler;
	public AudioClip DamageSound;

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Enemy") {
			GetComponent<AudioSource> ().PlayOneShot (DamageSound);
			LifeHandler.GetComponent<LifeManager>().Lives -= 1;
			Destroy(other.gameObject);
		}
	}
}
