﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebuffHandler : MonoBehaviour
{
	public List<Debuff> mydebuffs;
	private Debuff firedebuff;
	private Debuff colddebuff;

	void Update ()
	{
		firedebuff = null;
		colddebuff = null;

		foreach (Debuff debuff in mydebuffs) 
		{
			if (debuff != null)
			{
				debuff.PlayDebuff();
			}
			else if (debuff == null)
			{
				mydebuffs.Remove(debuff);
			}
			if (debuff.GetComponent<FireDebuff> () != null) {
				firedebuff = debuff;
			}
			if (debuff.GetComponent<ColdDebuff> () != null) {
				colddebuff = debuff;
			}
		}
	}

	public void AddDebuff (Debuff NewDebuff)
	{
		//If the new debuff is a fire debuff
		if (NewDebuff.GetComponent<FireDebuff> () != null) {
			//Check if I already have a fire debuff.
			//If I don't, apply it normally.
			if (firedebuff == null) {
				NewDebuff.SetTarget (gameObject);
				mydebuffs.Add (NewDebuff);
			}
			//If I do, apply the damage/duration of the stronger debuff.
			else {
				//If my current DOT is stronger, just refresh the timer to that of the new, weaker DoT.
				if (firedebuff.GetComponent<FireDebuff> ().Magnitude > NewDebuff.GetComponent<FireDebuff> ().Magnitude) {
					firedebuff.Duration = NewDebuff.Duration;
				}
				//But if the new DoT is stronger, copy it.
				else if (firedebuff.GetComponent<FireDebuff> ().Magnitude < NewDebuff.GetComponent<FireDebuff> ().Magnitude) {
					firedebuff.Magnitude = NewDebuff.Magnitude;
					firedebuff.Duration = NewDebuff.Duration;
				}
			}
		} else if (NewDebuff.GetComponent<ColdDebuff> () != null) {
			if (colddebuff == null) {
				NewDebuff.SetTarget (gameObject);
				mydebuffs.Add (NewDebuff);
			}
			//If I do, apply the damage/duration of the stronger debuff.
			else {
				//Compares magnitude of slow effects. Use whatever is stronger. Refresh duration if my current one is stronger.
				if (colddebuff.GetComponent<ColdDebuff> ().Magnitude > NewDebuff.GetComponent<ColdDebuff> ().Magnitude) {
					colddebuff.Duration = NewDebuff.Duration;
				}
				//But if the new DoT is stronger, copy it.
				else if (colddebuff.GetComponent<ColdDebuff> ().Magnitude < NewDebuff.GetComponent<ColdDebuff> ().Magnitude) {
					colddebuff.Magnitude = NewDebuff.Magnitude;
					colddebuff.Duration = NewDebuff.Duration;
				}
			}
		}
	}

	public bool IsBurning()
	{
		if (firedebuff != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public bool IsChilled()
	{
		if (colddebuff != null) {
			return true;
		} else {
			return false;
		}
	}
}
