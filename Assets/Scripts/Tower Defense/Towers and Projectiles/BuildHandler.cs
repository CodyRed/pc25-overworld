﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildHandler : MonoBehaviour {

	public GameObject Location;
	public GoldManager MyGold;

	public GameObject ArrowButton;
	public GameObject CannonButton;

	public GameObject ArrowTower;
	public GameObject CannonTower;

	public GameObject UpgradeButton;
	public GameObject SellButton;
	
	public Text InfoText;
	public Text DescText;

	public AudioClip BuildSound;
	public AudioClip SellSound;

	public GameObject PointerArrow;
	private GameObject newtower;


	void Update() {
		CheckMenu();
		if (Location != null) {
			PointerArrow.transform.position = new Vector3(Location.transform.position.x - 2.5f, 1.0f, 
				Location.transform.position.z + 2.5f);
			PointerArrow.transform.rotation = Quaternion.AngleAxis (225.0f, Vector3.up);
		}
	}
	
	public void SetSlot(GameObject slot)
	{
		Location = slot;
		Debug.Log ("My current buildslot is " + slot.name);
	}

	public void BuildArrowTower() {
		if (Location != null) {
			if (Location.GetComponent<Build>().HasTower == false)
			{
				gameObject.GetComponent<MoveTut> ().MoveTo30 ();
				if (MyGold.GetComponent<GoldManager>().MyGold >= ArrowTower.GetComponent<Cost>().GoldCost){
					GetComponent<AudioSource> ().PlayOneShot (BuildSound);
					newtower = Instantiate (ArrowTower, Location.transform.position + new Vector3 (0.0f, 0.5f, 0.0f), Quaternion.identity) as GameObject;
					newtower.transform.localEulerAngles = new Vector3 (0.0f, 180.0f, 0.0f);
					Location.GetComponent<Build> ().MyTower = newtower;
					Location.GetComponent<Build> ().MyTower.name = "Arrow Tower";
					Location.GetComponent<Build> ().HasTower = true;
					MyGold.GetComponent<GoldManager>().MyGold -= ArrowTower.GetComponent<Cost>().GoldCost;
					newtower.GetComponent<TowerLinker> ().MyBuildSpot = Location;
				}
				else {
					InfoText.text = ("You don't have enough gold!");
				}
			}
			else if (Location.GetComponent<Build>().HasTower == true)
			{
				InfoText.text = ("There's already a tower there!");
			}
		}
	}
	
	public void BuildCannonTower() {
		if (Location != null) {
			if (Location.GetComponent<Build>().HasTower == false)
			{
				gameObject.GetComponent<MoveTut> ().MoveTo30 ();
				if (MyGold.GetComponent<GoldManager>().MyGold >= CannonTower.GetComponent<Cost>().GoldCost){
					GetComponent<AudioSource> ().PlayOneShot (BuildSound);
					newtower = Instantiate (CannonTower, Location.transform.position + new Vector3 (0.0f, 0.5f, 0.0f), Quaternion.identity) as GameObject;
					newtower.transform.localEulerAngles = new Vector3 (0.0f, 180.0f, 0.0f);
					Location.GetComponent<Build> ().MyTower = newtower;
					Location.GetComponent<Build> ().MyTower.name = "Cannon Tower";
					Location.GetComponent<Build> ().HasTower = true;
					MyGold.GetComponent<GoldManager>().MyGold -= CannonTower.GetComponent<Cost>().GoldCost;
					newtower.GetComponent<TowerLinker> ().MyBuildSpot = Location;
				}
				else {
					InfoText.text = ("You don't have enough gold!");
				}
			}
			else if (Location.GetComponent<Build>().HasTower == true)
			{
				InfoText.text = ("There's already a tower there!");
			}
		}
	}

	public void Upgrade() {
		if (Location.GetComponent<Build> ().MyTower.GetComponent<Upgrade>().MaxLevel == false) {
			gameObject.GetComponent<MoveTut> ().MoveTo40 ();
			if (MyGold.GetComponent<GoldManager> ().MyGold >= 
				Location.GetComponent<Build> ().MyTower.GetComponent<Upgrade> ().Cost) {
				MyGold.GetComponent<GoldManager> ().MyGold -= 
					Location.GetComponent<Build> ().MyTower.GetComponent<Upgrade> ().Cost;
				Destroy (Location.GetComponent<Build> ().MyTower);
				newtower = Instantiate (Location.GetComponent<Build> ().MyTower.GetComponent<Upgrade> ().MyUpgrade, 
				                        Location.transform.position + new Vector3 (0.0f, 0.5f, 0.0f), 
				                        Quaternion.identity) as GameObject;
				newtower.name = Location.GetComponent<Build> ().MyTower.GetComponent<Upgrade> ().MyUpgrade.name;
				newtower.transform.localEulerAngles = new Vector3 (0.0f, 180.0f, 0.0f);
				Location.GetComponent<Build> ().MyTower = newtower;
				newtower.GetComponent<TowerLinker> ().MyBuildSpot = Location;
				GetComponent<AudioSource> ().PlayOneShot (BuildSound);
			} else {
				InfoText.text = ("You don't have enough gold!");
			}
		} else {
			InfoText.text = ("This tower is already at its max level!");
		}
	}

	public void Sell() {
		gameObject.GetComponent<MoveTut> ().MoveTo40 ();
		GetComponent<AudioSource> ().PlayOneShot (SellSound);
		MyGold.GetComponent<GoldManager>().MyGold += 
			Mathf.RoundToInt(Location.GetComponent<Build>().MyTower.GetComponent<Cost>().GoldCost/2);
		Destroy (Location.GetComponent<Build> ().MyTower);
		Location.GetComponent<Build> ().HasTower = false;
	}

	void CheckMenu() {
		if ((Location != null) && (Location.GetComponent<Build>().HasTower == false)) {
			ArrowButton.SetActive(true);
			CannonButton.SetActive(true);
			UpgradeButton.SetActive(false);
			SellButton.SetActive(false);
		} else if ((Location != null) && (Location.GetComponent<Build>().HasTower == true)) {
			ArrowButton.SetActive(false);
			CannonButton.SetActive(false);
			UpgradeButton.SetActive(true);
			SellButton.SetActive(true);
		}
	}
}
