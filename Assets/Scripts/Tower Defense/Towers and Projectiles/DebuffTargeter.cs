﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebuffTargeter : MonoBehaviour {

	public bool SpreadFire;
	public bool SpreadCold;

	public GameObject DebuffAttack(List<GameObject> victims)
	{
		foreach (GameObject enemy in victims)
		{
			if (SpreadFire)
			{
				if ((!enemy.GetComponent<DebuffHandler>().IsBurning()))
				{
					return enemy;
				}
			}
			else if (SpreadCold)
			{
				if ((!enemy.GetComponent<DebuffHandler>().IsChilled()))
				{
					return enemy;
				}
			}
		}
		return victims [0];
	}
}
