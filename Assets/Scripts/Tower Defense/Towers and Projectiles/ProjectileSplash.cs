﻿using UnityEngine;
using System.Collections;

public class ProjectileSplash : MonoBehaviour
{
	public GameObject Explosion;
	public float Speed;
	public float SplashZoneMaxRadius;
	public GameObject ExplosionSound;
	private Vector3 targetposition;
	private Debuff debuffobject;
	private Debuff mydebuff;

	void Start ()
	{
		//Lock projectile to hit the location, not the enemy.
		targetposition = gameObject.GetComponent<ProjectileTargeter> ().GetTarget ().transform.position;
		if (gameObject.GetComponent<DebuffAttack> () != null) {
			mydebuff = gameObject.GetComponent<DebuffAttack> ().MyDebuff;
		}
	}

	void Update ()
	{
		Vector3 direction = targetposition - transform.position;
		direction.Normalize ();
		transform.position += direction * Speed * Time.deltaTime;
		if (Vector3.Distance (targetposition, transform.position) < 0.33f) {
			
			GameObject[] Victims = GameObject.FindGameObjectsWithTag ("Enemy");
			{
				foreach (GameObject victim in Victims) 
				{
					if (Mathf.Abs (Vector3.Distance(transform.position, victim.transform.position)) <= SplashZoneMaxRadius)
					{
						victim.gameObject.GetComponent<HitPoints> ().Health -= ComputeSplashDamage(transform.position, victim.transform.position);
						if (mydebuff != null)
						{
							debuffobject = Instantiate(mydebuff, transform.position, Quaternion.identity) as Debuff;
							debuffobject.GetComponent<Debuff>().Initialize(gameObject.GetComponent<DebuffAttack>().Magnitude,
							                                               gameObject.GetComponent<DebuffAttack>().Duration);
							debuffobject.GetComponent<Debuff>().SetTarget(victim);
							victim.gameObject.GetComponent<DebuffHandler>().AddDebuff(debuffobject);
						}
					}
				}
			}
			Instantiate (ExplosionSound, transform.position, Quaternion.identity);
			Instantiate (Explosion, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}
	}

	float ComputeSplashDamage (Vector3 ExplosionCenter, Vector3 TargetPosition)
	{
		float basedmg = (Random.Range (gameObject.GetComponent<ProjectileDamage> ().GetMinDmg (), gameObject.GetComponent<ProjectileDamage> ().GetMaxDmg ()));
		float finaldmg = Mathf.Floor(basedmg / (Mathf.Floor(Vector3.Distance(ExplosionCenter, TargetPosition))));
		if (finaldmg > gameObject.GetComponent<ProjectileDamage> ().GetMaxDmg ()) 
		{
			finaldmg = gameObject.GetComponent<ProjectileDamage> ().GetMaxDmg ();
		}
		return finaldmg;
	}
}