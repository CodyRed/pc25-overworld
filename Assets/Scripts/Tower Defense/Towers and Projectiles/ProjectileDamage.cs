﻿using UnityEngine;
using System.Collections;

public class ProjectileDamage : MonoBehaviour {

    private float mindamage;
    private float maxdamage;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDamage(float min, float max)
    {
        mindamage = min;
        maxdamage = max;
        //Debug.Log ("My minimum damage is " + mindamage);
        //Debug.Log ("And my maximum damage is " + maxdamage);
    }

    public float GetMinDmg()
    {
        return mindamage;
    }

    public float GetMaxDmg()
    {
        return maxdamage;
    }
}
