﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Build : MonoBehaviour {

	public GameObject Manager;
	public GameObject MyTower;
	public bool HasTower;

	public Material EmptyTex;
	public Material OccupiedTex;

	public AudioClip SelectSound;
	public GameObject MyArrow;

	private Color myColor; 
	private float flashTimer;

	void Awake() {
		Manager = GameObject.Find ("Build Manager");
		myColor = gameObject.GetComponent<Renderer> ().material.color;
	}

	void Update() {
		if (HasTower) {
			gameObject.GetComponent<Renderer> ().material = OccupiedTex;
		} else {
			gameObject.GetComponent<Renderer> ().material = EmptyTex;
		}

		if (flashTimer > 0) {
			flashTimer -= 1 * Time.deltaTime;
			gameObject.GetComponent<Renderer> ().material.color = Color.white;
		}

		if (flashTimer <= 0) {
			gameObject.GetComponent<Renderer> ().material.color = myColor;
			flashTimer = 0;
		}
	}

	void OnMouseUpAsButton(){
		gameObject.GetComponent<MoveTut> ().MoveTo20 ();
		GetComponent<AudioSource> ().PlayOneShot (SelectSound);
		Manager.GetComponent<BuildHandler> ().SetSlot (gameObject);
		flashTimer = 0.5f;
	}
}
