﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TowerAttack : MonoBehaviour {

	public GameObject TowerProjectile;
	public float MinimumDamage;
	public float MaximumDamage;
	public float Cooldown;
	public float Range;
	public AudioClip AttackSound;

	private List<GameObject> targets = new List<GameObject>();
	private List<GameObject> reachabletargets = new List<GameObject> ();
	private GameObject mytarget;
	private GameObject currentbullet;
	private float cooldown;

	public bool isNotNative;

	public GameObject PauseMaster;
	public GameObject Patcher;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
		Patcher = GameObject.Find ("TDMaster");
	}

	void Update ()
	{
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == true) {
			return;
		}

		if (isNotNative == false) {
			if (Patcher != null) {
				if (Patcher.GetComponent<TDSwitches> ().isArrowTowerFixed == false) {
					return;
				}
			}
		}

		if (cooldown > 0) {
			cooldown -= 1 * Time.deltaTime;
		}
		
		if (cooldown < 0) {
			cooldown = 0;
		}
		SeekTarget();
		if (reachabletargets.Count != 0) {
			if (mytarget != null) {
				if (Mathf.Abs (Vector3.Distance (transform.position, mytarget.transform.position)) < Range) {
					if (cooldown <= 0) {
						GetComponent<AudioSource> ().PlayOneShot (AttackSound);
						currentbullet = Instantiate (TowerProjectile, transform.position, Quaternion.identity) as GameObject;
						currentbullet.GetComponent<ProjectileDamage> ().SetDamage (MinimumDamage, MaximumDamage);
						currentbullet.GetComponent<ProjectileTargeter> ().SetTarget (mytarget);
						cooldown = Cooldown;
					}
				}
			}
		}
	}

	void SeekTarget()
	{
		targets.Clear ();
		reachabletargets.Clear ();
		targets.AddRange (GameObject.FindGameObjectsWithTag ("Enemy"));
		targets.AddRange(GameObject.FindGameObjectsWithTag ("Boss"));

		if (gameObject.GetComponent<GroundOnly> () == null) {
			targets.AddRange(GameObject.FindGameObjectsWithTag ("Flier"));
		}

		foreach (GameObject enemy in targets) {
			if (Vector3.Distance (transform.position, enemy.transform.position) <= Range) {
				reachabletargets.Add (enemy);
			}
		}

		if (reachabletargets.Count != 0) {
			if (gameObject.GetComponent<DebuffTargeter> () != null) {
				mytarget = gameObject.GetComponent<DebuffTargeter> ().DebuffAttack (reachabletargets);
			} else {
				mytarget = reachabletargets [0];
			}
		}

		//However if that enemy is too far away (because of movement), flush the list (meaning, do nothing and re-evaluate situation)
		if (mytarget != null) {
			if (Vector3.Distance (transform.position, mytarget.transform.position) > Range) {
				reachabletargets.Clear ();
			}
		}
	}
}