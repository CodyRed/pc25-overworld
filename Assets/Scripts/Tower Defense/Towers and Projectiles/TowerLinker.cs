﻿using UnityEngine;
using System.Collections;

public class TowerLinker : MonoBehaviour {

	public AudioClip Beep;
	public GameObject Manager;
	public GameObject MyBuildSpot;

	void Start() {
		Manager = GameObject.Find ("Build Manager");
	}

	void OnMouseUpAsButton(){
		Manager.GetComponent<BuildHandler> ().SetSlot (MyBuildSpot);
		GetComponent<AudioSource> ().PlayOneShot (Beep);
	}
}
