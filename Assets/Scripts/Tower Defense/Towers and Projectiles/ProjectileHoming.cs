﻿using UnityEngine;
using System.Collections;

public class ProjectileHoming : MonoBehaviour
{
	public float Speed;
	private GameObject target;
	private float timedlife;

	void Start()
	{
		target = gameObject.GetComponent<ProjectileTargeter> ().GetTarget ();
		timedlife = 0.5f;
	}

	void Update ()
	{
		timedlife -= 1 * Time.deltaTime;

		if (target.gameObject == null) {
			Destroy(gameObject);
		}

		if (timedlife <= 0) {
			Destroy (gameObject);
		}

		if (gameObject.GetComponent<ProjectileTargeter> ().GetTarget() != null) {
			Vector3 direction = gameObject.GetComponent<ProjectileTargeter> ().GetTarget ().transform.position - transform.position;
			direction.Normalize ();
			transform.position += direction * Speed * Time.deltaTime;
		} else {
			Destroy(gameObject);
		}

		if (target.gameObject != null) {
			if (Vector3.Distance (transform.position, target.transform.position) <= 0.5f) {
				float finaldmg = Mathf.Floor (Random.Range (gameObject.GetComponent<ProjectileDamage> ().GetMinDmg (), gameObject.GetComponent<ProjectileDamage> ().GetMaxDmg ()));
				target.gameObject.GetComponent<HitPoints> ().Health -= finaldmg;
				Destroy (gameObject);
			}
		}
		else {
			Destroy(gameObject);
		}
	}
}
