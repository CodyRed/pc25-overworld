﻿using UnityEngine;
using System.Collections;

public class ProjectileTargeter : MonoBehaviour {

    private GameObject target;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SetTarget(GameObject Target)
    {
        target = Target;
    }

    public GameObject GetTarget()
    {
        return target;
    }
}
