﻿using UnityEngine;
using System.Collections;

public class Debuff : MonoBehaviour {

	public int Magnitude;
	public float Duration;

	protected GameObject target;

	public void SetTarget(GameObject NewTarget)
	{
		target = NewTarget;
	}

	public void Initialize(int strength, int time)
	{
		Magnitude = strength;
		Duration = time;
	}

	public virtual void PlayDebuff()
	{

	}

	public virtual void ClearDebuff()
	{

	}
}
