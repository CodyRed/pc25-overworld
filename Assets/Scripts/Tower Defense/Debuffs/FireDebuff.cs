﻿using UnityEngine;
using System.Collections;

public class FireDebuff : Debuff {
	
	public override void PlayDebuff ()
	{
		target.GetComponent<HitPoints>().Health -= Magnitude * Time.deltaTime;
		Duration -= 1 * Time.deltaTime;
		if (Duration <= 0) {
			CapHealth();
			ClearDebuff ();
		}
	}

	void CapHealth()
	{
		target.GetComponent<HitPoints> ().Health =
			Mathf.Round (target.GetComponent<HitPoints> ().Health);
	}

	public override void ClearDebuff()
	{
		Destroy (gameObject);
	}
}
