﻿using UnityEngine;
using System.Collections;

public class ColdDebuff : Debuff {

	private float origspeed;

	void Start()
	{
		origspeed = target.GetComponent<NavMeshAgent> ().speed;
	}

	public override void PlayDebuff ()
	{
		target.GetComponent<NavMeshAgent> ().speed = origspeed / Magnitude;
		Duration -= 1 * Time.deltaTime;
		if (Duration <= 0) {
			ClearDebuff ();
		}
	}
	
	public override void ClearDebuff()
	{
		target.GetComponent<NavMeshAgent> ().speed = origspeed;
		Destroy (gameObject);
	}
}
