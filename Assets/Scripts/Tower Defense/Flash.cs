﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Flash : MonoBehaviour {

	public Material red;
	public Material white;

	public float flashTimer = 10;

	void Start () {

	}

	void Update () {
		flashTimer -= 2.5f * Time.deltaTime;
		if (Mathf.RoundToInt(flashTimer) % 2 == 0) {
			gameObject.GetComponent<Renderer> ().material = red;
		} else if (flashTimer % 2 != 0) {
			gameObject.GetComponent<Renderer> ().material = white;
		}

		if (flashTimer <= 0) {
			flashTimer = 10;
		}
	}
}