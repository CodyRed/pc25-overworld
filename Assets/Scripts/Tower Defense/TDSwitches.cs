﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TDSwitches : MonoBehaviour {

	public bool isShipEnabled = false;
	public bool isArrowTowerFixed = false;
	public bool InfiniteLives = false;
	public bool InfiniteGold = false;
	public bool isTdComplete = false;
    private static bool createdtd = false;
    public static bool tddone = false;

    void Awake()
    {
        //DontDestroyOnLoad (gameObject);
        if (!createdtd)
        {
            // this is the first instance - make it persist
            DontDestroyOnLoad(this.gameObject);
            createdtd = true;
        }
        else {
            // this must be a duplicate from a scene reload - DESTROY!
            Destroy(this.gameObject);
        }
    }

    void Start () {
	
	}

    void Update()
    {
        if (isTdComplete)
        {
            tddone = true;
        }
    }

    public void StartTD() {
        SceneManager.LoadScene("TD");
    }

	public void EnableShip() {
		isShipEnabled = true;
	}

	public void DisableShip() {
		isShipEnabled = false;
	}

	public void FixArrowTower() {
		isArrowTowerFixed = true;
	}

	public void BreakArrowTower() {
		isArrowTowerFixed = false;
	}

	public void EnableInfiniteGold() {
		InfiniteGold = true;
	}

	public void DisableInfiniteGold() {
		InfiniteGold = false;
	}

	public void EnableInfiniteLives() {
		InfiniteLives = true;
	}

	public void DisableInfiniteLives() {
		InfiniteLives = false;
	}
}
