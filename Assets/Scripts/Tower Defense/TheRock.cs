﻿using UnityEngine;
using System.Collections;

public class TheRock : MonoBehaviour {

	public GameObject Victory;
	public GameObject BonusSound;
	public GameObject Explosion;
	public GameObject PauseMaster;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (gameObject.GetComponent<HitPoints> ().Health <= 0) {
			PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			Instantiate (BonusSound, transform.position, Quaternion.identity);
			Instantiate (Explosion, transform.position, Quaternion.identity);
			Victory.SetActive (true);
			Destroy (gameObject);
		}
	}
}
