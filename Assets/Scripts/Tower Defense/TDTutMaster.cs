﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TDTutMaster : MonoBehaviour {

	public GameObject Arrow;
	public Text TextA;
	public Text TextB;
	public Text TextC;
	public Text TextD;
	public Text TextE;
	public int Stage;
	public float CountdownTime;
	private bool Countdown;

	void Start() {
		TextA.gameObject.SetActive(false);
		TextB.gameObject.SetActive(false);
		TextC.gameObject.SetActive(false);
		TextD.gameObject.SetActive(false);
		TextE.gameObject.SetActive(false);
	}

	void Update() {
		//Click sign!;
		if (Stage == 10) {
			Arrow.transform.position = new Vector3 (-12.0f, -0.0f, 2.2f);
			TextA.gameObject.SetActive (true);
			//Build tower!
		} else if (Stage == 20) {
			Arrow.transform.position = new Vector3 (-12.0f, 0.0f, -9.2f);
			TextA.gameObject.SetActive (false);
			TextB.gameObject.SetActive (true);
			//Upgrade or sell!
		} else if (Stage == 30) {
			Arrow.transform.position = new Vector3 (-12.0f, 0.0f, -9.2f);
			TextA.gameObject.SetActive (false);
			TextB.gameObject.SetActive (false);
			TextC.gameObject.SetActive (true);
		} else if (Stage == 40) {
			Arrow.transform.position = new Vector3 (-7.0f, 0.0f, -9.2f);
			TextA.gameObject.SetActive (false);
			TextB.gameObject.SetActive (false);
			TextC.gameObject.SetActive (false);
			TextD.gameObject.SetActive (true);
		} else if (Stage == 50) {
			Arrow.transform.position = new Vector3 (23.4f, 0.0f, 21.2f);
			Countdown = true;
			TextA.gameObject.SetActive (false);
			TextB.gameObject.SetActive (false);
			TextC.gameObject.SetActive (false);
			TextD.gameObject.SetActive (false);
			TextE.gameObject.SetActive (true);
		}

		if (Countdown == true) {
			CountdownTime -= 1 * Time.deltaTime;
		}
		if (CountdownTime <= 0) {
			Arrow.SetActive (false);
			TextE.gameObject.SetActive(false);
			CountdownTime = 0;
		}
	}
}
