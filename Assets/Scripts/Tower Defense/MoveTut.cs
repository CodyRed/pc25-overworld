﻿using UnityEngine;
using System.Collections;

public class MoveTut : MonoBehaviour {

	public GameObject TutMaster;

	void Start() {
		TutMaster = GameObject.Find ("TutMaster");
	}

	public void MoveTo20() {
		if (TutMaster.GetComponent<TDTutMaster> ().Stage == 10) {
			TutMaster.GetComponent<TDTutMaster> ().Stage = 20;
		}
	}

	public void MoveTo30() {
		if (TutMaster.GetComponent<TDTutMaster> ().Stage == 20) {
			TutMaster.GetComponent<TDTutMaster> ().Stage = 30;
		}
	}

	public void MoveTo40() {
		if (TutMaster.GetComponent<TDTutMaster> ().Stage == 30) {
			TutMaster.GetComponent<TDTutMaster> ().Stage = 40;
		}
	}

	public void MoveTo50() {
		if (TutMaster.GetComponent<TDTutMaster> ().Stage == 40) {
			TutMaster.GetComponent<TDTutMaster> ().Stage = 50;
		}
	}
}
