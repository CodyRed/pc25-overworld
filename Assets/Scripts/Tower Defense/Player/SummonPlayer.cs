﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SummonPlayer : MonoBehaviour {

	public GameObject Player;
	public int Cost;
	public Text InfoText;
	public Text TimedLifeText;
	public GoldManager MyGold;
	public float ShipTimer;
	public bool isShipEnabled;
	public GameObject Patcher;
	public AudioClip Whoosh;
	private bool hasShip;
	public GameObject PauseMaster;

	void Start() {
		PauseMaster = GameObject.Find ("PauseMaster");
		Patcher = GameObject.Find ("TDMaster");
		if (Patcher != null) {
			isShipEnabled = Patcher.GetComponent<TDSwitches> ().isShipEnabled;
		} else {
			isShipEnabled = true;
		}
	}

	void Update() {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (ShipTimer > 0) {
				ShipTimer -= 1 * Time.deltaTime;
				TimedLifeText.text = "SHIP CRASHES IN: " + ShipTimer.ToString ("F2") + " SECONDS";
			} else if (ShipTimer <= 0) {
				hasShip = false;
				ShipTimer = 0;
				TimedLifeText.text = " ";
			}

			if (hasShip) {
				InfoText.text = "ARROWS and SPACEBAR!";
			}
		}
	}

	public void CallTheFleet() {
		gameObject.GetComponent<MoveTut> ().MoveTo50 ();

		if (hasShip == true) {
			InfoText.text = "You already have a ship!";
			return;
		}

		if (isShipEnabled == true) {
			if (MyGold.GetComponent<GoldManager> ().MyGold >= Cost) {
				GetComponent<AudioSource> ().PlayOneShot (Whoosh);
				hasShip = true;
				MyGold.GetComponent<GoldManager> ().MyGold -= Cost;
				ShipTimer = Player.GetComponent<TimedLife> ().Duration;
				Instantiate (Player, new Vector3 (0.0f, 0.0f, 0.0f), Quaternion.identity);
			} else {
				InfoText.text = "Not enough gold!";
			}
		} else {
			InfoText.text = "404: 'SHIP' not found! Where is it?";
		}
	}
}
