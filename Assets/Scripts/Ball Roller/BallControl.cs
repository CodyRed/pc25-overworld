﻿using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour
{

    public float speed;
    private int score = 0;

    private Rigidbody rb;

    public void scoreget()
    {
        Debug.Log("FUCK YEAH BRO");
        score = 0;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Debug.Log(score);
        if(score == 6)
        {
            scoreget();
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            score++;
        }
    }
}