﻿using UnityEngine;
using System.Collections;

public class ShopGuySwitches : MonoBehaviour {

	public bool isHappinessEasy = false;
	public bool isSecurityEnabled = false;
	private static bool createdsg = false;

	void Awake() {
		if (!createdsg)
		{
			DontDestroyOnLoad(this.gameObject);
			createdsg = true;
		} else {
			Destroy(this.gameObject);
		}
	}

	public void EnableEasyHappiness() {
		isHappinessEasy = true;
	}

	public void DisableEasyHappiness() {
		isHappinessEasy = false;
	}

	public void EnableSecurity() {
		isSecurityEnabled = true;
	}

	public void DisableSecurity() {
		isSecurityEnabled = false;
	}
}
