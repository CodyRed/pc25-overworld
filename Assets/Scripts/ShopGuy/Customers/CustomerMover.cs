﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomerMover: MonoBehaviour
{
	public List<GameObject> PossiblePaths = new List<GameObject>();
	public bool setDestination;
	public int TargetPoint;
	public Vector3 startPoint;
	public Vector3 destination;
	public bool isFrozen = true;

	private Vector3 target;

	void Start() {
		startPoint = transform.position;
	}

	void Update() {
		if (gameObject.GetComponent<Customer> ().pauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (isFrozen) {
				Debug.Log ("NOT MOVING.");
				return;
			}

			if (setDestination == false) {
				Debug.Log ("My target is point " + TargetPoint);
				if (PossiblePaths [TargetPoint].GetComponent<CustomerDetector> ().isOccupied == false) {
					setDestination = true;
					destination = new Vector3 (PossiblePaths [TargetPoint].transform.position.x, PossiblePaths [TargetPoint].transform.position.y, 0.0f);
				} else {
					return;
				}
			}

			if ((Vector3.Distance (transform.position, destination) >= 0.25) && (setDestination == true)
				&& (gameObject.GetComponent<Customer>().isDoneShopping == false)) {
				target = destination - transform.position;
				target.Normalize ();
				transform.position += target * 10.0f * Time.deltaTime;
			} else {
				gameObject.GetComponent<Customer> ().isInPlace = true;
			}

			if (gameObject.GetComponent<Customer> ().isDoneShopping == true) {
				gameObject.GetComponent<Customer> ().isInPlace = false;
				target = startPoint - transform.position;
				target.Normalize ();
				transform.position += target * 10.0f * Time.deltaTime;
				if (Vector3.Distance (transform.position, startPoint) <= 0.25) {
					Destroy (gameObject);
				}
			}
		}
	}

	public void HardReset() {
		setDestination = false;
		TargetPoint = -1;
		destination = new Vector3 (0.0f, 0.0f, 0.0f);
		target = new Vector3 (0.0f, 0.0f, 0.0f);
	}
}
