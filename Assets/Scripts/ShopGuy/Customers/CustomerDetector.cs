﻿using UnityEngine;
using System.Collections;

public class CustomerDetector : MonoBehaviour {

	public bool isOccupied;
	public bool isTargeted;
	private GameObject myCustomer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (isOccupied) {
			if (Vector3.Distance (transform.position, myCustomer.transform.position) > 1) {
				//I don't want to set it to null. This is different from "Wanted Item".
				isOccupied = false;
				myCustomer = gameObject;
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.GetComponent<Customer>() != null) {
			myCustomer = other.gameObject;
			isOccupied = true;
			isTargeted = false;
		}

		if ((isOccupied == true) && (other.gameObject != myCustomer) && (other.gameObject.tag != "IgnoreMe")) {
			other.gameObject.GetComponent<CustomerMover> ().HardReset ();
		}
	}
}
