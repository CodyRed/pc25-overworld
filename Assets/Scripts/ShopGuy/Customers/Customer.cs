﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Customer : MonoBehaviour {

	public string Name;
	public Item WantedItem;
	public int Weight;
	public List<Item> PossibleItems = new List<Item>();
	public float myPatience;
	public float patienceDecayIncrement = 1.0f;
	public bool isInPlace;
	public bool isDoneShopping;
	public bool isWaiting;
	public bool isEvilTaxCollector;
	public bool noPatienceLeft;
	public GameObject PatienceBar;
	public GameObject dataHandler;
	public GameObject pauseMaster;
	public GameObject happyBar;

	public AudioClip Success;
	public AudioClip Wait;
	public AudioClip Angry;
	public AudioClip Evil;

	private bool runOnce;
	public GameObject Shopkeeper;
	private float maxPatience;
	private GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("ShopGuyMaster");
		maxPatience = myPatience;
		pauseMaster = GameObject.Find ("PauseMaster");
		dataHandler = GameObject.FindGameObjectWithTag ("CustomerDataHandler");
		happyBar = GameObject.Find ("Happiness");
		if (PatienceBar != null) {
			PatienceBar.SetActive (false);
		}
		if (isEvilTaxCollector == true) {
			Shopkeeper = GameObject.Find ("Shopkeeper");
		}
	}

	void Update () {
		if (pauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (PatienceBar != null) {
				PatienceBar.transform.localScale = new Vector3 (myPatience / maxPatience, 
					PatienceBar.transform.localScale.y, PatienceBar.transform.localScale.z);
			}

			if (isDoneShopping) {
				if (isEvilTaxCollector == false) {
					Destroy (PatienceBar);
				}
			}

			if (isEvilTaxCollector == false) {
				if (WantedItem == null) {
					RequestItem ();
				}
			}

			if (isInPlace) {
				if (isEvilTaxCollector == true) {
					if (runOnce == false) {
						GetComponent<AudioSource> ().PlayOneShot (Evil);
						Shopkeeper.GetComponent<ShopCashHandler> ().Cash -= Mathf.RoundToInt (Shopkeeper.GetComponent<ShopCashHandler> ().Cash * 0.33f);
						runOnce = true;
						isDoneShopping = true;
					}
				}
					
				myPatience -= (1 * patienceDecayIncrement) * Time.deltaTime;
				if (isEvilTaxCollector == false) {
					PatienceBar.SetActive (true);
				}
			}

			if (myPatience <= 0) {
				noPatienceLeft = true;
				if (noPatienceLeft == false) {
					GetComponent<AudioSource> ().PlayOneShot (Angry);
					if (Patcher != null) {
						if (Patcher.GetComponent<ShopGuySwitches> ().isHappinessEasy == false) {
							happyBar.GetComponent<Happiness> ().CustomerHappiness -= Weight;
						} else {
							happyBar.GetComponent<Happiness> ().CustomerHappiness -= 2;
						}
					} else {
						happyBar.GetComponent<Happiness> ().CustomerHappiness -= Weight;
					}
					isDoneShopping = true;
					if (PatienceBar != null) {
						PatienceBar.SetActive (false);
					}
				}
			}
		}
	}

	//Rolls a random item on the list of possible items this customer could want.
	void RequestItem(){
		int RandomItem = Random.Range (0, PossibleItems.Count);
		WantedItem = PossibleItems[RandomItem];
	}

	void OnMouseUpAsButton(){
		if ((isInPlace == true) && (isDoneShopping == false)) {
			pauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			dataHandler.transform.position = dataHandler.GetComponent <SalesHandler> ().startPosition;
			dataHandler.GetComponent<SalesHandler> ().myCustomer = gameObject;
		}
	}
}