﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomerSpawner : MonoBehaviour {

	public List<GameObject> DestinationPoints = new List<GameObject>();
	public List<GameObject> CustomerList = new List<GameObject>();
	public int SpawnIterator;
	public float Cooldown;
	private float resetCooldown;
	private GameObject myCustomer;
	public GameObject pauseMaster;

	private GameObject Shopkeeper;
	public bool taxEnabled;

	void Start () {
		resetCooldown = Cooldown;
		Shopkeeper = GameObject.Find ("Shopkeeper");
		pauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		if (Shopkeeper.GetComponent<ShopCashHandler> ().Cash >= 5000) {
			taxEnabled = true;
		} else {
			taxEnabled = false;
		}

		if (pauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			IteratorChecker ();
			int toSpawn = 0;

			if (Cooldown > 0) {
				Cooldown -= 1 * Time.deltaTime;
			} else {
				if (taxEnabled == false) {
					toSpawn = Random.Range (0, CustomerList.Count - 2);
				} else {
					toSpawn = Random.Range (0, CustomerList.Count);
				}
				myCustomer = Instantiate (CustomerList [toSpawn].gameObject, transform.position, Quaternion.identity) as GameObject;
				Cooldown = Random.Range(resetCooldown - 5, resetCooldown + 5);
				CheckSpots (myCustomer);
			}
		}
	}

	void CheckSpots(GameObject customer) {
		if ((DestinationPoints[SpawnIterator].GetComponent<CustomerDetector>().isTargeted == false) || (DestinationPoints[SpawnIterator].GetComponent<CustomerDetector>().isOccupied == false)) 
		{
			DestinationPoints [SpawnIterator].GetComponent<CustomerDetector> ().isTargeted = true;
			myCustomer.GetComponent<CustomerMover> ().PossiblePaths = DestinationPoints;
			myCustomer.GetComponent<CustomerMover>().TargetPoint = SpawnIterator;
			myCustomer.GetComponent<CustomerMover> ().isFrozen = false;
			SpawnIterator++;
			IteratorChecker ();
		} else {
			SpawnIterator++;
			IteratorChecker ();
		}
	}

	void IteratorChecker() {
		if (SpawnIterator >= DestinationPoints.Count) {
			SpawnIterator = 0;
		}
	}
}
