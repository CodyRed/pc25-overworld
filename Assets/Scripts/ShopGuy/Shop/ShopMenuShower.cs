﻿using UnityEngine;
using System.Collections;

public class ShopMenuShower : MonoBehaviour {

	public GameObject MenuToShow;

	public void ShowMenu() {
		MenuToShow.SetActive (true);
	}
}
