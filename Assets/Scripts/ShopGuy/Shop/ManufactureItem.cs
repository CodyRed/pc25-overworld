﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ManufactureItem : MonoBehaviour {

	public List<Item> CraftableItems = new List<Item>();
	public Item WantedItem;
	public bool isMakingItem;
	public Text InfoText;
	private float buildTime;
	public float buildTimeBonus;
	private GameObject PauseMaster;

	// Use this for initialization
	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
	}
	
	// Update is called once per frame
	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			if (WantedItem != null) {
				InfoText.text = "Making " + WantedItem.ItemName;
				CreateItem ();
			}

			if (buildTime > 0) {
				buildTime -= 1 * Time.deltaTime;
			} else if ((buildTime <= 0) && (isMakingItem == true)) {
				InfoText.text = WantedItem.ItemName + " complete!";
				GetItem ();
			}
		}
	}

	public void CreateItem(){
		if (isMakingItem == false) {
			if ((WantedItem.MetalComponentCost <= gameObject.GetComponent<ShopResources> ().MetalComponent) &&
			    (WantedItem.BioComponentCost <= gameObject.GetComponent<ShopResources> ().BioComponent) &&
			    (WantedItem.CrystalComponentCost <= gameObject.GetComponent<ShopResources> ().CrystalComponent) &&
			    (WantedItem.CarbonComponentCost <= gameObject.GetComponent<ShopResources> ().CarbonComponent) &&
			    (WantedItem.OilComponentCost <= gameObject.GetComponent<ShopResources> ().OilComponent)) {
				gameObject.GetComponent<ShopResources> ().MetalComponent -= WantedItem.MetalComponentCost;
				gameObject.GetComponent<ShopResources> ().BioComponent -= WantedItem.BioComponentCost;
				gameObject.GetComponent<ShopResources> ().CrystalComponent -= WantedItem.CrystalComponentCost;
				gameObject.GetComponent<ShopResources> ().CarbonComponent -= WantedItem.CarbonComponentCost;
				gameObject.GetComponent<ShopResources> ().OilComponent -= WantedItem.OilComponentCost;
				isMakingItem = true;
				buildTime = WantedItem.BuildTime - buildTimeBonus;
				if (buildTime <= 0) {
					buildTime = 0.3f;
				}
			} else {
				InfoText.text = "Not enough resources!";
			}
		}
	}

	public void SetWantedItem(Item item) {
		WantedItem = item;
	}

	void GetItem() {
		gameObject.GetComponent<ShopInventory> ().MyInventory.Add (WantedItem);
		WantedItem = null;
		isMakingItem = false;
	}
}
