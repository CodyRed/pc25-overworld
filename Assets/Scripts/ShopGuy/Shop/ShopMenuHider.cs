﻿using UnityEngine;
using System.Collections;

public class ShopMenuHider : MonoBehaviour {

	public GameObject MenuToHide;

	public void HideMenu() {
		MenuToHide.SetActive (false);
	}
}