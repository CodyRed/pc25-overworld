﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopResources : MonoBehaviour {

	public int MetalComponent;
	public int BioComponent;
	public int CarbonComponent;
	public int CrystalComponent;
	public int PlateComponent;
	public int OilComponent;
	public float MetalTime = 4.0f;
	public float maxMetalTime;
	public float BioTime = 4.0f;
	public float maxBioTime;
	public float CarbonTime = 4.0f;
	public float maxCarbonTime;
	public float CrystalTime = 0.0f;
	public float maxCrystalTime = 7.0f;
	public float OilTime = 0.0f;
	public float maxOilTime = 10.0f;
	public float PlateTime = 0.0f;
	public float maxPlateTime = 8.0f;

	private int CooldownDecreaseCount = 1;
	private int CraftTimeDecreaseCount = 1;

	public float CraftTimeBonus;
	public float CooldownBonus;

	public Text InfoText;

	private GameObject PauseMaster;

	// Use this for initialization
	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
		maxMetalTime = MetalTime;
		maxCarbonTime = CarbonTime;
		maxBioTime = BioTime;
		maxCrystalTime = CrystalTime;
		maxPlateTime = PlateTime;
		maxOilTime = OilTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (PauseMaster.GetComponent<MasterPauser> ().isPaused == false) {
			Cooldown ();
			Recharge ();
		}
	}

	void Recharge() {
		if (MetalTime <= 0) {
			MetalComponent++;
			MetalTime = maxMetalTime;
		}

		if (CarbonTime <= 0) {
			CarbonComponent++;
			CarbonTime = maxCarbonTime;
		}

		if (BioTime <= 0) {
			BioComponent++;
			BioTime = maxBioTime;
		}

		if (CrystalTime <= 0) {
			CrystalComponent++;
			CrystalTime = maxCrystalTime;
		}

		if (PlateTime <= 0) {
			PlateComponent++;
			PlateTime = maxPlateTime;
		}

		if (OilTime <= 0) {
			OilComponent++;
			OilTime = maxOilTime;
		}
	}

	void Cooldown() {
		MetalTime -= 1 * Time.deltaTime;
		CarbonTime -= 1 * Time.deltaTime;
		BioTime -= 1 * Time.deltaTime;
		PlateTime -= 1 * Time.deltaTime;
		CrystalTime -= 1 * Time.deltaTime;
		OilTime -= 1 * Time.deltaTime;
	}
		
	public void DecreaseMetalCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxMetalTime - 0.5f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxMetalTime -= 0.5f;
			} else {
				InfoText.text = "Metal - max cooldown already!";
			}
		}
	}
		
	public void DecreaseCarbonCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxCarbonTime - 0.5f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxCarbonTime -= 0.5f;
			} else {
				InfoText.text = "Carbon - max cooldown already!";
			}
		}
	}

	public void DecreaseBioCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxBioTime - 0.5f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxBioTime -= 0.5f;
			} else {
				InfoText.text = "Bio - max cooldown already!";
			}
		}
	}

	public void DecreaseCrystalCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxCrystalTime - 0.8f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxCrystalTime -= 0.8f;
			} else {
				InfoText.text = "Crystal - max cooldown already!";
			}
		}
	}

	public void DecreasePlateCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxPlateTime - 1.0f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxPlateTime -= 1.0f;
			} else {
				InfoText.text = "Plate - max cooldown already!";
			}
		}
	}

	public void DecreaseOilCooldown() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 1000) {
			if (maxOilTime - 1.0f > 0) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 1000;
				CooldownDecreaseCount++;
				maxOilTime -= 1.0f;
			} else {
				InfoText.text = "Oil - max cooldown already!";
			}
		}
	}

	public void DecreaseBuildTime() {
		if (gameObject.GetComponent<ShopCashHandler> ().Cash >= 5000) {
			if (CraftTimeDecreaseCount <= 5) {
				gameObject.GetComponent<ShopCashHandler> ().Cash -= 5000;
				CraftTimeDecreaseCount++;
				gameObject.GetComponent<ManufactureItem> ().buildTimeBonus += 2.5f;
			} else {
				InfoText.text = "Max build speed already!";
			}
		}
	}
}
