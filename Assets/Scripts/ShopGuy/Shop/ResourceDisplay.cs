﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResourceDisplay : MonoBehaviour {

	public Text MetalQty;
	public Text CarbonQty;
	public Text BioQty;
	public Text CrystalQty;
	public Text OilQty;
	public Text PlateQty;
	public GameObject ResourcePile;

	void Start () {
		ResourcePile = GameObject.Find ("Shopkeeper");
	}

	void Update () {
		MetalQty.text = "Metal: " + ResourcePile.GetComponent<ShopResources> ().MetalComponent;
		CarbonQty.text = "Carbon: " + ResourcePile.GetComponent<ShopResources> ().CarbonComponent;
		BioQty.text = "Bio: " + ResourcePile.GetComponent<ShopResources> ().BioComponent;
		CrystalQty.text = "Crystal: " + ResourcePile.GetComponent<ShopResources> ().CrystalComponent;
		OilQty.text = "Oil: " + ResourcePile.GetComponent<ShopResources> ().OilComponent;
		PlateQty.text = "Plate: " + ResourcePile.GetComponent<ShopResources> ().PlateComponent;
	}
}
