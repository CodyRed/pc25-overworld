﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopCurrentInventoryDisplay : MonoBehaviour {

	public Text RationQty;
	private int RationCount;
	public Text StickQty;
	private int StickCount;
	public Text RivetQty;
	private int RivetCount;
	public Text FrameQty;
	private int FrameCount;
	public Text PlateQty;
	private int PlateCount;
	public Text GunkQty;
	private int GunkCount;

	public GameObject ShopKeeper;

	// Update is called once per frame
	void Update () {
		foreach (Item checkitem in ShopKeeper.GetComponent<ShopInventory>().MyInventory) {
			if (checkitem.GetComponent<Item> ().ItemName == "O.K Ration") {
				RationCount++;
			} else if (checkitem.GetComponent<Item> ().ItemName == "Crystal Rivet") {
				RivetCount++;
			} else if (checkitem.GetComponent<Item> ().ItemName == "Wooden Stick") {
				StickCount++;
			} else if (checkitem.GetComponent<Item> ().ItemName == "Crystal Frame") {
				FrameCount++;
			} else if (checkitem.GetComponent<Item> ().ItemName == "Alloy Plate") {
				PlateCount++;
			} else if (checkitem.GetComponent<Item> ().ItemName == "Oily Gunk") {
				GunkCount++;
			}
		}

		RationQty.text = "x " + RationCount.ToString ();
		StickQty.text = "x " + StickCount.ToString ();
		RivetQty.text = "x " + RivetCount.ToString ();
		PlateQty.text = "x " + PlateCount.ToString ();
		GunkQty.text = "x " + GunkCount.ToString ();
		FrameQty.text = "x " + FrameCount.ToString ();

		RationCount = 0;
		RivetCount = 0;
		StickCount = 0;
		GunkCount = 0;
		PlateCount = 0;
		FrameCount = 0;
	}
}
