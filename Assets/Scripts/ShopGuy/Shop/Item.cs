﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

	public string ItemName;
	public int Price;
	public float BuildTime;
	public int MetalComponentCost;
	public int BioComponentCost;
	public int CarbonComponentCost;
	public int CrystalComponentCost;
	public int OilComponentCost;
}
