﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SalesHandler : MonoBehaviour {
	public GameObject myCustomer;
	public Vector3 startPosition;
	public Vector3 hidePosition;
	public Text myWantedText;
	public Text quantityText;
	public GameObject ShopKeeper;

	private GameObject happyBar;
	private GameObject pauseMaster;
	private Item wantedItem;
	private GameObject Patcher;

	void Start() {
		Patcher = GameObject.Find ("ShopGuyMaster");
		ShopKeeper = GameObject.Find ("Shopkeeper");
		happyBar = GameObject.Find ("Happiness");
		startPosition = transform.position;
		hidePosition = new Vector3 (transform.position.x + 1000.0f, 0.0f, 0.0f);
		pauseMaster = GameObject.Find ("PauseMaster");
		transform.position = hidePosition;
	}

	void Update() {
		if (Input.GetKey(KeyCode.Escape)) {
			this.transform.position = hidePosition;
		}

		if (myCustomer != null) {
			myWantedText.text = "I want a " + myCustomer.GetComponent<Customer> ().WantedItem.ItemName + "!";
			wantedItem = myCustomer.GetComponent<Customer> ().WantedItem;
			int ItemQuantity = 0;
			foreach (Item checkitem in ShopKeeper.GetComponent<ShopInventory>().MyInventory) {
				if (checkitem.ItemName == myCustomer.GetComponent<Customer> ().WantedItem.ItemName) {
					ItemQuantity++;
				}
			}
			quantityText.text = "You have: " + ItemQuantity;
		}
	}

	public void SellItem() {
		if (ShopKeeper.GetComponent<ShopInventory> ().MyInventory.Contains (wantedItem)) {
			ShopKeeper.GetComponent<ShopInventory> ().MyInventory.Remove (wantedItem);
			ShopKeeper.GetComponent<ShopCashHandler> ().Cash += (wantedItem.Price + Mathf.RoundToInt(wantedItem.Price * happyBar.GetComponent<Happiness>().HappyBonus));
			happyBar.GetComponent<Happiness>().CustomerHappiness += Mathf.RoundToInt(myCustomer.GetComponent<Customer>().Weight/2);
			myCustomer.GetComponent<AudioSource> ().PlayOneShot (myCustomer.GetComponent<Customer> ().Success);
			myCustomer.GetComponent<Customer> ().isDoneShopping = true;
			myCustomer.GetComponent<Customer> ().myPatience = 1000;
			pauseMaster.GetComponent<MasterPauser> ().isPaused = false;
			this.transform.position = hidePosition;
		} else {
			myWantedText.text = "You don't have any of that item.";
		}
	}

	public void Wait() {
		if (myCustomer.GetComponent<Customer> ().isWaiting == false) {
			myCustomer.GetComponent<AudioSource> ().PlayOneShot (myCustomer.GetComponent<Customer> ().Wait);
			myCustomer.GetComponent<Customer> ().isWaiting = true;
			myCustomer.GetComponent<Customer> ().patienceDecayIncrement = 0.5f;
			if (Patcher != null) {
				if (Patcher.GetComponent<ShopGuySwitches> ().isHappinessEasy == false) {
					happyBar.GetComponent<Happiness> ().CustomerHappiness -= 1;
				}
			}
		}
			pauseMaster.GetComponent<MasterPauser> ().isPaused = false;
			this.transform.position = hidePosition;
	}

	public void Dismiss() {
		ShopKeeper.GetComponent<ShopCashHandler> ().Cash -= myCustomer.GetComponent<Customer> ().Weight * 2;
		myCustomer.GetComponent<AudioSource> ().PlayOneShot (myCustomer.GetComponent<Customer> ().Angry);
		if (Patcher != null) {
			if (Patcher.GetComponent<ShopGuySwitches> ().isHappinessEasy == true) {
				happyBar.GetComponent<Happiness> ().CustomerHappiness -= 3;
			} else {
				happyBar.GetComponent<Happiness> ().CustomerHappiness -= 8;
			}
		} else {
			happyBar.GetComponent<Happiness> ().CustomerHappiness -= 8;
		}
		myCustomer.GetComponent<Customer>().isDoneShopping = true;
		pauseMaster.GetComponent<MasterPauser> ().isPaused = false;
		this.transform.position = hidePosition;
	}
}
