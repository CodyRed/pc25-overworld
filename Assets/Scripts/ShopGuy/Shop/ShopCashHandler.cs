﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopCashHandler : MonoBehaviour {

	public float Cash;
	public Text CurrentCashDisplay;

	void Start () {
	
	}

	void Update () {
		CurrentCashDisplay.text = "Current cash: " + Cash;
	}
}
