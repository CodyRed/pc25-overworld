﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HappinessIcon : MonoBehaviour {

	public Texture BestFace;
	public Texture GoodFace;
	public Texture NeutFace;
	public Texture BadFace;
	public Texture WorstFace;
	public GameObject HappinessBar;

	void Start () {
	
	}

	void Update () {
		if (HappinessBar.GetComponent<Happiness> ().CustomerHappiness <= 20) {
			gameObject.GetComponent<RawImage> ().texture = WorstFace;
		} else if ((HappinessBar.GetComponent<Happiness> ().CustomerHappiness >= 21) &&
		           (HappinessBar.GetComponent<Happiness> ().CustomerHappiness <= 40)) {
			gameObject.GetComponent<RawImage> ().texture = BadFace;
		} else if ((HappinessBar.GetComponent<Happiness> ().CustomerHappiness >= 41) &&
		           (HappinessBar.GetComponent<Happiness> ().CustomerHappiness <= 60)) {
			gameObject.GetComponent<RawImage> ().texture = NeutFace;	
		} else if ((HappinessBar.GetComponent<Happiness> ().CustomerHappiness >= 61) &&
		           (HappinessBar.GetComponent<Happiness> ().CustomerHappiness <= 80)) {
			gameObject.GetComponent<RawImage> ().texture = GoodFace;
		} else if (HappinessBar.GetComponent<Happiness> ().CustomerHappiness >= 81) {
			gameObject.GetComponent<RawImage> ().texture = BestFace;
		}
	}
}
