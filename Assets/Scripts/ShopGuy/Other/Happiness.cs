﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Happiness : MonoBehaviour {

	public Image HappyBar;
	public Image EmptyBar;
	public float CustomerHappiness;
	public float HappyBonus;
	private float maxHappiness = 100.0f;
	public GameObject Victory;
	public GameObject Defeat;
	private GameObject PauseMaster;

	void Start () {
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	void Update () {
		DrawHappiness ();
		HappyBonus = CustomerHappiness * 0.25f;

		if (CustomerHappiness <= 0) {
			PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			Defeat.SetActive (true);
		} else if (CustomerHappiness > 100) {
			CustomerHappiness = 100;
		}

		if (CustomerHappiness >= 90) {
			PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			Victory.SetActive (true);
		}
	}

	void DrawHappiness() {
		HappyBar.rectTransform.localScale = new Vector3(CustomerHappiness/maxHappiness, 
			HappyBar.transform.localScale.y, HappyBar.transform.localScale.z);

		EmptyBar.rectTransform.localScale = new Vector3(maxHappiness/maxHappiness, 
			EmptyBar.transform.localScale.y, EmptyBar.transform.localScale.z);		
	}
}