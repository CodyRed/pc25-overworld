﻿using UnityEngine;
using System.Collections;

public class BuyPassword : MonoBehaviour {

	public GameObject BonusMenu;
	private GameObject Player;
	private GameObject PauseMaster;

	// Use this for initialization
	void Start () {
		Player = GameObject.Find ("Shopkeeper");
		PauseMaster = GameObject.Find ("PauseMaster");
	}

	public void GetPassword(){
		if (Player.GetComponent<ShopCashHandler> ().Cash >= 20000) {
			Player.GetComponent<ShopCashHandler> ().Cash -= 20000;
			PauseMaster.GetComponent<MasterPauser> ().isPaused = true;
			BonusMenu.SetActive (true);
		}
	}
}
