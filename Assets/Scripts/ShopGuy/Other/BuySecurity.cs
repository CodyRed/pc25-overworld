﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuySecurity : MonoBehaviour {

	public GameObject BuildSpot;
	public GameObject SecurityTower;
	public Text InfoText;
	public Text MyText;

	private GameObject Shopkeeper;
	private bool AlreadyBought;
	private GameObject Patcher;

	void Start () {
		Patcher = GameObject.Find ("ShopGuyMaster"); 
		Shopkeeper = GameObject.Find ("Shopkeeper");
	}

	void Update() {
		if (Patcher != null) {
			if (Patcher.GetComponent<ShopGuySwitches> ().isSecurityEnabled == false) {
				MyText.text = "[Error] 'Security' not found!";
				return;
			}
		}

		if (AlreadyBought == true) {
			transform.position = new Vector3 (3000.0f, 0.0f, 0.0f);
			MyText.text = "Security: Already bought!";
		}
	}
	
	public void InstallSecurity() {
		if (Patcher.GetComponent<ShopGuySwitches> ().isSecurityEnabled == true) {
			if (AlreadyBought == false) {
				if (Shopkeeper.GetComponent<ShopCashHandler> ().Cash >= 4000) {
					Shopkeeper.GetComponent<ShopCashHandler> ().Cash -= 4000;
					Instantiate (SecurityTower, BuildSpot.transform.position, Quaternion.identity);
					AlreadyBought = true;
				} else {
					InfoText.text = "Not enough cash!";
				}
			}
		}
	}
}