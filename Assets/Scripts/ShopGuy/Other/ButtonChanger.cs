﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonChanger : MonoBehaviour {

	public Image ButtonImage;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<Button>().image = ButtonImage;
	}
}
