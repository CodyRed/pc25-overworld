﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnableBuying : MonoBehaviour {

	public GameObject SPatcher;
	public Text MyText;
	public Text InfoText;
	public GameObject Shopkeeper;

	void Start () {
		Shopkeeper = GameObject.Find ("Shopkeeper");
		SPatcher = GameObject.Find ("SShooterMaster");
	}
	
	public void EnableShopping() {
		if (Shopkeeper.GetComponent<ShopCashHandler> ().Cash >= 8000) {
			Shopkeeper.GetComponent<ShopCashHandler> ().Cash -= 8000;
			SPatcher.GetComponent<SShooterSwitches> ().isShoppingEnabled = true;
			MyText.text = "Advertising: We're on the air!";
		} else {
			InfoText.text = "Not enough cash!";
		}
	}
}
