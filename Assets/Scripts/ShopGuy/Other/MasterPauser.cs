﻿using UnityEngine;
using System.Collections;

public class MasterPauser : MonoBehaviour {

	public bool isPaused;

	public void PauseWorld() {
		isPaused = true;
	}

	public void UnpauseWorld() {
		isPaused = false;
	}
}
