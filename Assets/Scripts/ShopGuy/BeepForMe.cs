﻿using UnityEngine;
using System.Collections;

public class BeepForMe : MonoBehaviour {

	public AudioClip Beep;

	public void BeepMe() {
		GetComponent<AudioSource> ().PlayOneShot (Beep);
	}
}
