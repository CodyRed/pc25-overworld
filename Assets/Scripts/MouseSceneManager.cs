﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using Fungus;

public class MouseSceneManager : MonoBehaviour{

    //RightMenu Holder
    public GameObject RightClick;
    public GameObject Holder;

    //DOUBLE CLICK
    private bool doubleclick;
    private float lastClickTime;
    private float catchTime = 0.25f;
    private bool RightClickEnable = true;

    //mouse over
    public Button rightopenbutton;
    private int opentype;
    public bool pc25over { get; set; }
    public bool TDover { get; set; }
    public bool Spaceover { get; set; }
    public bool RecycleBin { get; set; }
    public bool ballroller { get; set; }
    public bool pong { get; set; }
    public bool helloworld { get; set; }

    //arranger
    public GameObject icon1;
    public GameObject icon2;
    public GameObject icon3;
    public GameObject icon4;
    public GameObject icon5;
    public GameObject icon6;
    public GameObject icon7;
    public GameObject icon8;
    public GameObject icon9;
    public GameObject icon10;
    public GameObject icon11;
    public GameObject icon12;
    public GameObject position1;
    public GameObject position2;
    public GameObject position3;
    public GameObject position4;
    public GameObject position5;
    public GameObject position6;
    public GameObject position7;
    public GameObject position8;
    public GameObject position9;
    public GameObject position10;
    public GameObject position11;
    public GameObject position12;

    /// fungus
    public Flowchart functionsflowchart;

    public void callmepls()
    {
        Debug.Log("IT FUCKING WORKS YO");
    }

    public void arrrangepls()
    {
        icon1.transform.position = position1.transform.position;
        icon2.transform.position = position2.transform.position;
        icon3.transform.position = position3.transform.position;
        icon4.transform.position = position4.transform.position;
        icon5.transform.position = position5.transform.position;
        icon6.transform.position = position6.transform.position;
        icon7.transform.position = position7.transform.position;
        icon8.transform.position = position8.transform.position;
        icon9.transform.position = position9.transform.position;
        icon10.transform.position = position10.transform.position;
        icon11.transform.position = position11.transform.position;
        icon12.transform.position = position12.transform.position;
        rightclickclose();
    }

    void turnon()
    {
        RightClickEnable = true;
    }

    void turnoff()
    {
        RightClickEnable = false;
    }

    private void rightclick()
{
        if(RightClickEnable)
        {

            if (RecycleBin)
            {
                opentype = 1;
            }
            if (Spaceover)
            {
                opentype = 2;
            }
            if (TDover)
            {
                opentype = 3;
            }
            if (ballroller)
            {
                opentype = 4;
            }
            if (pong)
            {
                opentype = 5;
            }
            if (helloworld)
            {
                opentype = 6;
            }
            if (pc25over)
            {
                opentype = 12;
            }
            ///sets right menu location
            Holder.transform.position = Input.mousePosition;
            RightClick.SetActive(true);
        }
        else
        {
            Debug.Log("still works lol");
        }
     
}

    public void rightopen()
    {
        if (opentype == 1)
        {
            Recyclebin();
        }
        if (opentype == 2)
        {
            SpaceShooter();
        }
        if (opentype == 3)
        {
            TD();
        }
        if (opentype == 4)
        {
            BallRoller();
        }
        if (opentype == 5)
        {
            Ponger();
        }
        if (opentype == 6)
        {
            shoppu();
        }
        if (opentype == 11)
        {
            helllloooo();
        }
        if (opentype == 12)
        {
            PC25();
        }

        rightclickclose();
    }

    public void rightclickclose()
    {
        opentype = 0;
        RightClick.SetActive(false);
    }

    public void Recyclebin()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("Recycle Bin");
        }

    }

    public void PC25()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("PC 25 Icon");
        }
        

    }

    public void TD()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("TD Icon");
        }

    }

    public void BallRoller()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("BallRoller Icon");
        }

    }

    public void Ponger()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("Pong Icon");
        }

    }

    public void SpaceShooter()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("Space Shooter");
        }

    }

    public void helllloooo()
    {
        if (doubleclick || opentype > 0)
        {
            Debug.Log("Hello World");
        }      
    }

    public void shoppu()
    {
        if (doubleclick || opentype > 0)
        {
            functionsflowchart.ExecuteBlock("ShopGuy");
        }
    }

    // Use this for initialization
    void Start () {
}

// Update is called once per frame
void Update () {

        ///Right Clicker
        if (Input.GetMouseButtonDown(1)){
            rightclick();    
        }

        ///enables or disables open command
        if(opentype > 0)
        {
            rightopenbutton.interactable = true;
        }
        else
        {
            rightopenbutton.interactable = false;
        }
        ///Debug.Log(opentype);

        ///Double Click Checker
        if (Input.GetButtonDown("Fire1"))
    {
        if (Time.time - lastClickTime < catchTime)
        {
            //double click
            //print("Double click");
            doubleclick = true;
        }
        else
        {
            //normal click
            //print("Single click");
            doubleclick = false;
        }
        lastClickTime = Time.time;
    }

    }

}
