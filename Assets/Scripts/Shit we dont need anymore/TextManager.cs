﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextManager : MonoBehaviour {

    public GameObject TextBox;
    public Text thetext;
    public Text history;

    public TextAsset textfile;
    public string[] textLines;
    public int currentLine;
    public int endAtLine;

    public bool ChatisActive = false;
    public bool listentome = false;

    private DesktopControl MasterControlScript;

    ///autotyping thing
    private bool istalking = false;
    private bool canceltalking = false;
    public float typespeed;

    // Use this for initialization
    void Start()
    {
        MasterControlScript = GetComponent<DesktopControl>();
        //masterCommandScript = GetComponent<DesktopControl>();
        ///if there is a textfile do the thing.
        if (textfile != null)
        {
            textLines = (textfile.text.Split('\n'));
        }

        ////scans the entirety of the textfile
        if(endAtLine == 0)
        {
            endAtLine = textLines.Length - 1;
        }

        ///if chat is activate initiate protocols and shiz
        if(ChatisActive)
        {
            EnableBox();
        }
        else
        {
            DisableBox();
        }
    }

    void Update()
    {
        if(!ChatisActive)
        {
            return;
        }

        ///thetext.text = textLines[currentLine];
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!istalking)
            {
                currentLine += 1;
                if (currentLine > endAtLine)
                {
                    listentome = false;
                    DisableBox();
                }
                else
                {
                    StartCoroutine(Scrollingthingy(textLines[currentLine]));
                }
            }
            else if(istalking && !canceltalking)
            {
                canceltalking = true;
            }
        }
    }

    private IEnumerator Scrollingthingy (string lineofText)
    {
        int letter = 0;
        thetext.text = "";
        istalking = true;
        canceltalking = false;
        while (istalking && !canceltalking && (letter < lineofText.Length - 1))
        {
            thetext.text += lineofText[letter];
            letter += 1;
            yield return new WaitForSeconds(typespeed);
        }
        thetext.text = lineofText;
        istalking = false;
        canceltalking = false;
     }

    public void EnableBox()
    {
        TextBox.SetActive(true);
        ChatisActive = true;
        currentLine = 0;

        if (listentome)
        {
            MasterControlScript.Imustlisten = true;
        }
        StartCoroutine(Scrollingthingy(textLines[currentLine]));
    }

    public void DisableBox()
    {
        TextBox.SetActive(false);
        ChatisActive = false;

        if (!listentome)
        {
            MasterControlScript.Imustlisten = false;
        }
    }

    /*public void ReloadScript(TextAsset Datext)
    {
        if(Datext != null)
        {
            textLines = new string[1];
            textLines = (thetext.text.Split('\n'));
        }
    }*/
}
