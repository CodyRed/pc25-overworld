﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Iamcomeplete : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
    void Done()
    {
        SceneManager.LoadScene("GAME OVER");
    }

	// Update is called once per frame
	void Update () {
        if (SShooterSwitches.ssdone && TDSwitches.tddone)
            Done();
    }
}
