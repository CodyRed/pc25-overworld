﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOvr : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SShooterSwitches.ssdone = false;
        TDSwitches.tddone = false;

    }
	
    void Done()
    {
        SceneManager.LoadScene("GAME OVER");
    }

	// Update is called once per frame
	void Update () {
        if (SShooterSwitches.ssdone && TDSwitches.tddone)
            Done();
    }
}
